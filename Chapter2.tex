\begin{savequote}[60mm]
"Hasta el infinito y más allá ..."
\qauthor{-- Buzz Lightyear}
\end{savequote}

\chapter{Objetivos}\label{sec:previos}


\section{Requerimientos de diseño}

En \NEW , tal y como se ha explicado en el capítulo anterior, la energía del evento se mide mediante un plano de fotomultiplicadores ubicados detrás de un cátodo transparente. La electrónica se debía diseñar e implementar para cumplir con estrictos requisitos: una resolución de energía global inferior al 1\% y un límite de radiopureza de $\SI{20}{\milli\becquerel\per\unit}$ en la cadena del \BI\ , por ello todos los componentes y materiales debían ser cuidadosamente seleccionados para asegurar un bajo nivel de radioactividad y al mismo tiempo cumplir con las especificaciones electrónicas requeridas para el \textit{FEE}. Los estudios de radiopureza están detallados en el capítulo \ref{sec:radiopureza} .% Los PMTs elegidos son de la compañia Hamamatsu, modelo R11410-10. Dichos PMTs fueron elegidos debido a su baja radioactividad.


\section{Conexión a tierra del cátodo del PMT y sus consecuencias.}

Para reducir los efectos de ruido de baja frecuencia y mejorar la seguridad del detector, se decidió utilizar una conexión de cátodo a tierra para los PMTs. Esto implica una lectura acoplada en AC y variaciones de la línea de base en las señales de los PMTs. En los próximos capítulos se presenta una descripción detallada de la electrónica y un enfoque novedoso basado en una restauración de la línea de base digital para obtener una respuesta lineal y manejar, de esa manera, los efectos de acoplamiento en AC. % El diseño final del canal del PMT se ha caracterizado con una linealidad mejor que del 0,4\% y un ruido por debajo de 0,4 mV.

En NEW, los fotocátodos de los PMTs (y, por lo tanto, sus carcasas) están conectados a tierra, mientras que el ánodo se establece a la tensión de funcionamiento de PMT (\NewPMTOperatingVoltage).
Esta solución simplifica la mecánica del detector y aumenta la seguridad (la alternativa, con el cátodo y el cuerpo de PMT a alta tensión habría requerido el aislamiento de cada PMT de tierra). A cambio, la salida del ánodo debe estar acoplada en AC a través de un condensador de acoplo, como se muestra en \figu\ \ref{fig:grounded_cathode}, ya que la tensión DC de la salida del ánodo es igual a la alta tensión que se aplica al PMT. Aunque se utilizará una línea de transmisión diferencial (línea TX) en la implementación final, los equivalentes de un solo extremo se utilizarán en las siguientes explicaciones por razones de simplicidad. Como consecuencia, $Z_{in}$ está relacionado con la impedancia equivalente de entrada de un solo extremo del amplificador, que se traducirá a una diferencial en el diseño final.

\begin{figure}[!h]
\centering
\includegraphics[width=0.55\textwidth]{IMG/Chapter2/grounded_cathode2.png}
\caption{Esquema de conexión del PMT con fotocátodo a tierra.}
\label{fig:grounded_cathode}
\end{figure}

Un esquema de acoplamiento AC crea, en primer orden, un filtro paso alto (HPF). La  \figu\ \ref{fig:GC_equivalent_circuit} muestra el circuito eléctrico equivalente del esquema de conexión, modelando el PMT como una fuente de corriente simple. 

\begin{figure}[!h]
\centering
\includegraphics[width=0.55\textwidth]{IMG/Chapter2/FEE_simple.png} 
\caption{Circuito equivalente con el cátodo conectado a tierra.}
\label{fig:GC_equivalent_circuit}
\end{figure}

La función de transferencia de Laplace del filtro (\figu\ \ref{fig:GC_equivalent_circuit}) se obtiene mediante las siguientes ecuaciones:

\begin{equation}
\frac{v_O}{i_I}=A\frac{R_1 Z_{in} C_2s}{1 + (R_1 + Z_{in})C_2s}
\label{eq.hpf}
\end{equation}

%\begin{equation}
%\frac{v_O}{i_I}=A\frac{R_1 Z_{in} C_2s}{1 + (R_1 + Z_{in})C_2s}\frac{(R_1 + Z_{in})}{(R_1 + Z_{in})}
%\label{eq.hpf}
%\end{equation}

\begin{equation}
\frac{v_O}{i_I}=A\frac{Z_{in}R_1}{Z_{in}+R_1}\frac{(R_1+Z_{in})C_2s}{1+(R_1+Z_{in})C_2s}
\label{eq.hpf}
\end{equation}

donde R$_1$ es la resistencia de alto valor que define principalmente el filtro junto con el condensador de desacoplo C$_2s$.

La terminación de la línea diferencial (120$\Omega$) se implementará como el doble de una línea de un solo extremo equivalente con una $Z_{in}$ de 60$\Omega$. Esta opción también permite terminar la señal de modo común que viaja a lo largo de la línea de transmisión y se espera que sea bastante alta ya que no se está utilizando una transmisión totalmente diferencial.

El filtro bloquea el componente DC y atenúa los componentes de frecuencia por debajo de la frecuencia de corte \FC\ , definida como:

\begin{equation}
f_{cutoff} = \frac{1}{2\pi(R_1+Z_{in})C_2}
\label{eq.hpf2}
\end{equation}

El pulso de salida después del filtro es la derivada del pulso de entrada y se caracteriza por un área total nula. Como la energía es proporcional al área del pulso de entrada, se deduce que se debe aplicar un algoritmo de deconvolución (o restauración de la línea de base) al pulso de salida para recuperar el área del pulso de entrada y así medir la energía. Además, como la resolución de energía intrínseca en \NEW\ es muy buena, el error introducido por el algoritmo de deconvolución debe ser, como máximo, una fracción por mil.

De hecho, el modelo simple de los PMT como fuente de corriente ideal es insuficiente dada la precisión requerida y debe ser refinado de varias maneras.
Por ejemplo, el efecto del intercambio de carga entre los condensadores de la base del PMT y el condensador de acoplo, debe incluirse en el modelo (\figu\ \ref{fig:FEE_PMT}). 

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.55\textwidth]{IMG/Chapter2/FEE_PMT_simple.png}
	\caption{\small Modelo más sofisticado del FEE + base del PMT.}
	\label{fig:FEE_PMT}
\end{figure}

El filtro resultante es más complicado que un HPF simple, pero aún puede describirse con precisión como una combinación de polo/cero obtenida del circuito equivalente que se muestra en \figu\ \ref{fig:FEE_PMT}. El filtro sigue la ecuación:

\begin{equation}
\frac{v_O}{i_I} =A\frac{Z_{in}}{(1+\frac{C_1}{C_2})}\frac{1+R_1C_1s}{1+\frac{(R_1+Z_{in})C_1}{(1+\frac{C1}{C2})}s} \label{eq:full_fee}
\end{equation}



%\begin{figure}[!htbp]
%	\centering
%	\subfloat[Effect in a square pulse.]{\includegraphics[width=0.45\textwidth]{pool/imgEP/pulse2.pdf}}
%	\vfill
%	\subfloat[Effect in a train of square pulses.]{\includegraphics[width=0.45\textwidth]{pool/imgEP/pulses2.pdf}}
%	\caption{The effect of a HPF.}
%	\label{fig:pulses}
%\end{figure}

\section{Condensadores de acoplo}

Para simplificar la construcción e integración del plano de energía en la estructura de la vasija, la carcasa del PMT y, por lo tanto, su conexión de cátodo se conectan directamente a la masa de cobre de la vasija. Este hecho implica que se debe utilizar un esquema de conexión de cátodo a tierra, ya que toda la estructura debe estar conectada a tierra por razones de seguridad eléctrica.

En una configuración de este tipo, tal como se ha explicado anteriormente, la salida de ánodo debe estar acoplada en AC a través de un condensador de acoplo, ya que la tensión de DC de salida de ánodo es igual a la alta tensión (HV) que se aplica al PMT. La información relevante sobre los condensadores de acoplo está más detallada en la sección \ref{cap:tau}.


\section{Linealidad}

El requisito más importante para el plano de energía es asegurar una buena linealidad en todo el rango dinámico. Para establecer los casos más restrictivos para la linealidad, se han realizado simulaciones de MonteCarlo. Los resultados mostraron que para el detector \NEW\ en condiciones de operación nominales, la peor situación en términos de linealidad fue para señales largas que podrían extenderse por más de \SI{100}{\micro\second}. En este caso, el circuito base del PMT debe proporcionar suficiente carga para el PMT con poco o nada de cambio de tensión entre los dínodos. Los cambios en estas tensiones introducen una ganancia variable en el tiempo que actúa como un mecanismo de distorsión no lineal y tiene un fuerte efecto en el comportamiento y la linealidad de la PMT. Los estudios de linealidad se muestran en la sección \ref{cap:lin}.


% -------------------------------------------------
% -------------------------------------------------
% -------------------------------------------------
