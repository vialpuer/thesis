\begin{savequote}[60mm]
"Nada en la vida es para ser temido, es solo para ser comprendido. Ahora es el momento de entender m\'as, de modo que podamos temer menos"
\qauthor{-- Marie Curie}
\end{savequote}

\chapter{Primeros resultados de energía de \NEW}\label{sec:resultados0}

Los primeros resultados de la resolución de energía obtenido es en el detector \NEW\ se muestran en \parencite{Renner:2018ttw} . A continuación se describen los resultados.

\section{Selección y reconstrucción de eventos.}

Los estudios presentados aquí se centran en eventos producidos en la región del pico fotoeléctrico $^{137}$Cs (661 keV) y en la región del pico $^{208}$Tl de " doble escape" (1592.5 keV). Este último corresponde a eventos en los que el gamma 2615 keV emitido en una desintegración de $^{208}$Tl se convierte en un campo eléctrico de un núcleo en un par electrón-positrón, y los dos rayos gamma 511 keV producidos por la aniquilación del positrón escapan sin ser detectados.
Las interacciones de la radiación ionizante en el xenón pueden ocasionalmente expulsar electrones de las capas internas de los átomos de xenón. Las transiciones posteriores de electrones en estas capas inferiores conducen a la emisión de rayos X característicos, varios de los cuales tienen energías cercanas a 30 keV. Algunos de estos rayos X pueden viajar una distancia significativa desde la pista de ionización principal antes de interactuar, o incluso escapar completamente del detector. Los rayos X que interactúan proporcionan una fuente adicional de líneas casi monoenergéticas que pueden usarse para la calibración.

\section{Selección de eventos.}

Las características del evento se calcularon a partir de las formas de onda del PMT adquirida. Las señales $S_1$ y $_2$ se identificaron como picos en la forma de onda PMT sumada (muestreadas en \textit{bins} de ancho 25 ns) en función de su ubicación y duración en el tiempo (ver \parencite{Martinez:2018ibw}
para más detalles). Los eventos con exactamente un pico $S_1$ y al menos uno pero no más de tres picos $S_2$ fueron aceptados para su posterior análisis.

\section{Medidas de energía}

La forma de onda de las señales PMT sumadas se integró en intervalos de 2 µs correspondientes al tiempo de tránsito de un electrón a través del intervalo EL. El patrón de las señales de SiPM observadas en cada uno de los segmentos de tiempo se usó para determinar el número y (\textit{x}, \textit{y}) posiciones de depósitos de energía separados, o "grupos", presentes en el intervalo de tiempo dado. Si se reconstruyó más de un grupo en un determinado intervalo de tiempo, la energía detectada por los PMT se distribuyó proporcionalmente entre los grupos de acuerdo con sus cargas, según lo determinado por las señales de SiPM. Las coordenadas espaciales (\textit{x}, \textit{y}, \textit{z}) de cada grupo se utilizaron para determinar el factor de corrección de energía necesario para la no uniformidad de respuesta en (\textit{x}, \textit{y}) y la vida útil de los electrones, como se describe en \parencite{Martinez:2018ibw} .

La energía total del evento se define como la suma de las energías de todos los grupos reconstruidos en el evento. La distribución de energía resultante de eventos reconstruidos en el volumen fiducial completo se muestra en la \figu\ \ref{distri}. El volumen fiducial completo incluye la mayor parte del volumen activo del detector y se define como $50 mm < Z_{min}$, $Z_{max} < 500 mm$ y $R_{max} < 180 mm$, donde $Z_{min}$ y $Z_{max}$ son las coordenadas z mínimas y máximas, y $R_{max}$ es la coordenada radial máxima de todos los grupos reconstruidos en un evento determinado. Son visibles varios picos que se analizarán con más detalle más adelante, incluido el foto pico de $^{137}$Cs y el $^{208}$Tl $e^+$ $e^-$ doble-pico de escape. El \textit{fotopico} $^{208}$Tl también es visible en los datos, pero se adquirieron pocos eventos, ya que las pistas a tales energías son demasiado largas para ser contenidas constantemente dentro del TPC a la presión de operación actual. Debido a esto, un análisis detallado de este pico de foto no es práctico en este conjunto de datos y se dejará para un estudio futuro a mayor presión. Tenga en cuenta que aunque el disparador se configuró para adquirir eventos por encima de aproximadamente 250 keV, todavía era posible examinar grupos aislados de energía depositados dentro de eventos de mayor energía, y por lo tanto también pudimos analizar los picos de energía correspondientes a los rayos X característicos del xenón.


\begin{figure}[!htbp]
	\centering
		\includegraphics[width=0.9\textwidth]{IMG/Chapter9/distri}
		\caption{Distribución energética de eventos registrados en toda la región fiducial. Los picos fotoeléctricos $^{137}$Cs (\textasciitilde 662 keV) y $^{208}$Th (\textasciitilde 2.6 MeV) son visibles junto con sus espectros Compton. Un pico también es visible a 1.6 MeV debido a la producción de par $e^+ e^-$ a partir de la gama de gamma de 2.6 MeV y el escape de ambos gammas de 511 keV producidos en la aniquilación de positrones resultante.}
		\label{fig:distri}
\end{figure}



\section{Distribución espacial de eventos y cortes fiduciales restringidos.}

La \figu\ \ref{fig:distri} indica la presencia de las líneas de calibración de interés, aunque con antecedentes significativos. La pureza de la muestra de calibración puede mejorarse aprovechando la localización espacial de las muestras fuente primarias. Como se muestra en la \figu\ \ref{fig:distri2}, la mayoría de los eventos en las líneas monoenergéticas se encuentran cerca de las posiciones de origen. Para mejorar la relación señal / ruido de la muestra de calibración, se aplica un corte fiducial, $160 mm < Z_{min}$, $Z_{max} < 300 mm$, en el análisis posterior. 

La respuesta del detector se degrada significativamente en los radios exteriores del volumen activo debido a la recolección imperfecta de luz por parte de los PMT \parencite{Martinez:2018ibw}, lo que contribuye significativamente a la degradación de la resolución energética. Para eliminar el sesgo inducido por los eventos que se aproximan a los bordes del detector, se aplica el corte fiducial $R_{max} < 150 mm$.

\begin{figure}[!htbp]
	\centering
		\includegraphics[width=0.9\textwidth]{IMG/Chapter9/distri2}
		\caption{Distribuciones de ubicaciones de eventos promedio en (\textit{x}, \textit{y}) (izquierda) y \textit{z} (derecha), para eventos en la región del pico de rayos x de xenón $\in$ (28, 32) keV (arriba), el pico fotoeléctrico $^{137}$Cs, $\in$ (650, 675) keV (centro) y eventos en la región del pico de doble escape, $\in$ (1550, 1640) keV (abajo). Las líneas rojas continuas indican las regiones fiduciales restringidas.}
		\label{fig:distri2}
\end{figure}


\section{Calibración de energía absoluta}

La energía del evento Q, determinada integrando la señal $S_2$ medida, se expresa en fotoelectrones utilizando factores de conversión determinados por la calibración de los PMT. Para determinar la escala de energía absoluta, la respuesta del detector medida se comparó con los valores nominales de las energías del evento. Se utilizaron tres muestras de eventos para este propósito: el pico de rayos X de xenón de 29.7 keV, el pico de $^{137}$Cs y el pico de escape doble. Las energías nominales se tomaron de \parencite{NuclearData} e incluyen el promedio ponderado por la intensidad de las líneas de rayos X $K\alpha$ (29.669 keV), la energía de los $^{137}$Cs emitidos gamma (661.6 keV) y la energía del pico de escape doble (1592.5 keV).
Se realizó un ajuste lineal para obtener energía calibrada E a partir de energía Q no calibrada como

\begin{equation}
E = a_0 \cdot Q + a_1
\end{equation}

y se encontró que los parámetros ajustados eran $a_0 = 3.2154 \pm 0.0003$ eV/fotoelectrón y $a_1 = 0.49975 \pm 0.01359$ keV. El ajuste se muestra en la \figu\ \ref{cali} junto con los residuos r definidos como la diferencia porcentual de la energía calibrada E de la energía nominal $E_0$, $r = 100 \cdot (E_0 - E (Q))$ / $E_0$, para cada punto $(Q, E_0)$ utilizado en el ajuste. Se consideró que los errores en las energías brutas medidas Q en el ajuste eran los errores en los valores medios de los componentes gaussianos de los ajustes a los tres picos en los fotoelectrones, y se encontró que estos errores eran inferiores al 0,05 \% de sus respectivos medios. La calibración demuestra que la respuesta del detector es lineal dentro de \textasciitilde 1 \% en el rango de energía de 29.7 keV a 1592.5 keV.

%
\begin{figure}[!htbp]
	\centering
		\includegraphics[width=1\textwidth]{IMG/Chapter9/cali}
		\caption{Calibración de energía en \NEW\ resultante de un ajuste lineal a tres energías medidas en un rango que corresponde aproximadamente a 1.6 MeV.}
		\label{fig:cali}
\end{figure}



\section{Resolución de energía}

A partir de una muestra de eventos monoenergéticos, la resolución de energía se puede determinar como la relación de FWHM de la distribución de energía al valor medio de la respuesta. A bajas energías, las deposiciones de energía son casi puntuales y la resolución energética está dominada por el término estocástico reflejando las fluctuaciones en la producción de pares de iones de electrones. La medición de energía para eventos de alta energía implica una suma de las deposiciones de energía sobre el volumen más grande del detector. Deben corregirse para la variación de la respuesta debido a la vida útil del electrón y/o las inhomogeneidades del detector y las imperfecciones residuales de las correcciones pueden contribuir a la resolución de la energía (un error sistemático). La importancia relativa de varias contribuciones puede estimarse mediante la comparación de la resolución energética observada a diferentes energías: se espera que el término estocástico siga a la dependencia 1/$\sqrt{E}$ de la energía, mientras que se espera que la contribución sistemática sea independiente de la energía. Por lo tanto, se espera que la variación de la resolución energética con energía sea:

\begin{equation}
R(E) = \sqrt{(a/\sqrt{E})^2 + c^2}
\end{equation}

o

\begin{equation}
R^2(E) = a^2/E + c^2
\end{equation}

donde $a/\sqrt{E}$ es el término estocástico y $c$ es el término sistemático y constante. Se espera que las contribuciones potenciales de ruido, que se comporten como $b/E$, sean insignificantes en nuestro caso.












