\begin{savequote}[60mm]
"Nada en la vida es para ser temido, es solo para ser comprendido. Ahora es el momento de entender m\'as, de modo que podamos temer menos"
\qauthor{-- Marie Curie}
\end{savequote}

\chapter{Tratamiento de la se\~nal (BLR)}\label{sec:BLR}

Para una correcta interpretación de los datos, era necesario implementar un algoritmo que restaurara la línea de base así como la forma del pulso, ya que, al pasar la señal por el condensador de acoplo esta cambia de forma. En este capítulo se desarrolla la implementación de dicho algoritmo.

\section{Algoritmo de la restauración de la linea base}
\label{Digi_base_res}

El algoritmo de restauración de línea de base (BLR) se basa en la implementación de la función inversa de un HPF (\figu\ \ref{fig:FEE_check}). La respuesta al impulso de esta función inversa tiene una estructura compuesta por un delta en el origen más una función escalonada cuya amplitud es igual al valor de $\frac{1}{\tau}$ donde $\tau$ es $(R_1+Z_{in}).C$~(equation \ref{eq:imp}). Esto significa que la operación de convolución se puede llevar a cabo utilizando solo un acumulador y un multiplicador en lugar de un filtro de respuesta de impulso finito (FIR) más complejo.

\begin{eqnarray}
HPF^{-1}(s)&=1+\frac{1}{\tau.s} \nonumber \\
HPF^{-1}(t)&=\delta(t)+\frac{1}{\tau} \int_{0}^{t} dt
\label{eq:imp}
\end{eqnarray}

\begin{figure}[!htbp]
	\centering
	\subfloat[HPF Respuesta al impulso inverso ]{\includegraphics[width=0.45\textwidth]{IMG/Chapter5/Impulse_R.png}}
	\subfloat[Inplementación MAC]{\includegraphics[width=0.45\textwidth]{IMG/Chapter5/Impulse_MAC.png}}
	\caption{Implementación inversa de HPF.}
	\label{fig:FEE_check}
\end{figure}     

Una buena característica de este algoritmo es que puede activarse cuando se detecta un pulso y luego apagarse cuando el pulso termina. Esto significa que una cantidad considerable de ruido de baja frecuencia filtrado por el condensador de acoplo AC no se volverá a introducir en el sistema, ya que es más lento que la longitud del pulso.
Por otro lado, el cero de baja frecuencia introducido por la interacción de la base del PMT agrega una cantidad baja de DC a la señal de salida teóricamente acoplada en AC del FEE. Para cancelar este efecto, se introduce un filtro paso alto con una frecuencia de corte igual a la frecuencia de este cero anterior al algoritmo BLR. Esto cancela completamente el efecto DC para que la señal reconstruida no muestre un cambio en la línea de base al final.

Cualquier algoritmo basado en un simple acumulador es sensible al \textbf{"non-zero mean noise"}. En particular, los componentes residuales de baja frecuencia cuyos periodos de componentes son más largos que la ventana de muestreo (ventana de tiempo de señal completa) son propensos a dejar un pequeño residuo dentro del acumulador. También con señales de pulso cortas la diferente altura y longitud de los lóbulos positivos y negativos resultantes después del acoplo AC, se traduce en \textbf{"non-zero mean error" }debido a los efectos de cuantificación en la conversión analógica a digital. Como consecuencia, el propio acumulador introduce un efecto de cambio de línea de base debido a estos \textbf{"non-zero mean noises"}.


El algoritmo BLR desarrollado para el plano de energía de \NEW\ controla el aumento del acumulador utilizando un mecanismo para agotar suavemente el residuo que queda en el acumulador después de una reconstrucción del pulso. El diagrama de flujo del algoritmo se muestra en \figu\ \ref{fig:BLR_acc_algor}. Se observó que en este algoritmo, el proceso de reconstrucción se inicia independientemente del comienzo del pulso y, en principio, puede estar activo de forma continua. El control se basa en las operaciones del acumulador que pueden ser {\em carga} o {\em descarga}. Cuando la señal sin procesar (proveniente del DAQ) se eleva por encima de un umbral o el valor del acumulador está por encima de otro umbral (que es la condición para un impulso activo), el acumulador se actualiza como en el algoritmo original. Sin embargo, cuando no se cumple ninguna de esas condiciones, lo que significa que no hay un impulso de señal activo, el acumulador se ve forzado a un estado de descarga controlada. Esta operación de descarga se lleva a cabo siguiendo una curva suave para que la señal reconstruida no muestre saltos o discontinuidades. Como ejemplo de la acción del algoritmo BLR, \figu\ \ref{fig:graph_sig} muestra la señal de salida de un PMT (que muestra el balanceo negativo característico introducido por el filtro) y la señal corregida después del algoritmo BLR.

\begin{figure}[H]
	\centering
		\includegraphics[width=0.9\textwidth]{IMG/Chapter5/BLR_acc_algo.pdf}
		\caption{Acumulador basado en BLR.}
		\label{fig:BLR_acc_algor}
\end{figure}

 
\begin{figure}[H]
	\centering
	\subfloat[Rojo - Señal de entrada real; Verde - señal de salida FEE]{\includegraphics[width=0.8\textwidth]{IMG/Chapter5/signal_preblr_test.pdf}}
	\hfill
	%\subfloat[Blue - Real input signal; Orange - FEE output signal]{\includegraphics[width=0.5\textwidth]{pool/imgEP/signal_preblr_test2.pdf}}

	\subfloat[Reconstrucción digital de base aplicada.]{\includegraphics[width=0.8\textwidth]{IMG/Chapter5/signal_blr.pdf}}
	\caption{Señal de simulación de pulso de 50 $\mu$s. 25865 pe. Noise 0.74 LSB}
	\label{fig:graph_sig}
\end{figure} 

El residual estimado en la corrección de energía se ha calculado aplicando la deconvolución a las señales de Monte Carlo generadas usando la simulación detallada descrita anteriormente y el efecto se ha cuantificado para que sea más pequeño que 0.3 \% FWHM para señales largas (correspondientes a grandes energías), y por lo tanto introduciendo un efecto más pequeño que la resolución debido al Fano factor.

\subsection{Implementación del sistema de adquisición de datos.}
\label{sec.atca}
 
En el sistema de adquisición de NEXT, los módulos DAQ basados en FPGA funcionan en modo \textbf{free-running}, almacenando datos continuamente en un búfer circular, mientras que un módulo de \textbf{trigger} del DAQ procesa los \textbf{triggers} candidatos de activación recibidos, generando una señal de \textbf{trigger} de activación que causa datos para ser enviados a una granja de PCs. NEXT ha adoptado el sistema de lectura escalable (SRS) en el ATCA (arquitectura de computación de telecomunicaciones avanzada) para su detector \NEW\ . El SRS-ATCA fue definido por la colaboración del CERN RD51 como una plataforma de lectura escalable de múltiples canales para una amplia gama de front-ends \parencite{1748-0221-11-01-C01008}.

 Se generan candidatos de trigger para cada canal de los PMTs en los módulos de datos del DAQ, ya que cada canal PMT puede detectar la luz de centelleo primaria y/o secundaria producida en la cámara. La generación de triggers candidatos desencadenantes se basa en la estimación de energía temprana de los eventos, que requiere una línea de base estable %\parencite{1748-0221-7-12-C12001}.

 Como consecuencia, la restauración de la línea de base digital debe implementarse \textbf{online}.

Se ha implementado una versión similar del algoritmo de restauración de línea de base digital (DBLR), introducido en la subsección \ref{Digi_base_res} y se muestra en \figu\ \ref{fig:BLR_acc_algor}. Este bloque DBLR se activa cada vez que la señal de entrada supera un umbral, lo que produce una señal de salida con su línea de base completamente restaurada. El umbral se define utilizando la línea de base de la señal como una referencia que requiere un mecanismo de cálculo de línea de base preciso. Se ha utilizado un filtro de promedio móvil en esta implementación. Con el fin de evitar los cambios en la línea de base debido a los residuos que quedan en el acumulador, y simplificar aún más la lógica y el uso de los recursos FPGA en los módulos DAQ y de activación, el mecanismo de control utiliza una función de suavizado lineal simple. Al igual que en el algoritmo original, en caso de que el acumulador no esté completamente vacío cuando finalice el proceso de reconstrucción, se debe limpiar automáticamente.

El algoritmo DBLR se ha implementado en una FPGA Xilinx Virtex-6 (XC6VLX240T-1ff1156). Hay un bloque DBLR por canal (12 canales PMT por módulo ATCA-FEC), y un filtro de limpieza anterior, ambos configurables. El algoritmo implementado y el filtro de limpieza se han implementado en un formato de punto fijo de 42 bits (Q11.30). El formato ha sido seleccionado como un compromiso entre la estabilidad del algoritmo y los recursos físicos utilizados.

\subsection{Deconvolución de las formas de onda sin procesar de PMT en datos de calibración de \Kr{83m}}

\NEW\ ha estado operando en el LSC desde octubre de 2016. El detector ha sido calibrado con datos tomados con una fuente de \Rb{83} conectada a su sistema de gas \parencite{Monrabal:2018xlr} . El isótopo exótico de rubidio decae a \Kr{83m} a través de la captura de electrones con una vida de \RbLifetime. El criptón luego decae al estado fundamental al emitir dos electrones de energía total \textasciitilde 40 keV, con una relación de ramificación del 95 \%. Una descomposición de Kr da como resultado un depósito de energía con una energía lo suficientemente baja para ser considerado como un punto.

%The total released energy sums up to \KrEnergy. A \Kr{83m} decay results, therefore in a point-like energy deposition. 

\begin{figure}[tbh!]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{IMG/Chapter5/kr_pmt_wfs.png} 
    \includegraphics[width=0.8\textwidth]{IMG/Chapter5/kr_pmt_wfs_zoom_s2.png} 
    \caption{\label{fig:rwf} Formas de onda de \Kr{83m} sin procesar para los PMTs individuales, mostrando la oscilación negativa introducido por la electrónica del front-end de los PMTs. La imagen superior muestra la forma de onda sin procesar en la ventana de adquisición de datos completa, mientras que la imagen inferior muestra un zoom en la señal \textbf{EL-amplified} EL (\st) en la que se activó la lectura del evento.}
  \end{center}
\end{figure}

\begin{figure}[tbh!]
  \begin{center}
    \includegraphics[width=0.7 \textwidth]{IMG/Chapter5/kr_sum_wf.png} 
    \includegraphics[width=0.7\textwidth]{IMG/Chapter5/kr_sum_wf_zoom_s1.png}
    \includegraphics[width=0.7\textwidth]{IMG/Chapter5/kr_sum_wf_zoom_s2.png} 
    \caption{\label{fig:cwf} Formas de onda de \Kr{83m} corregidas para la suma de los PMTs. La imagen superior muestra la forma de onda corregida en la ventana de adquisición de datos completa (\so y \st), mientras que la imagen central muestra los zooms de la señal de centelleo principal (\so) y la imagen inferior muestra las formas de onda de la señal \textbf{EL-amplified} (\st).}
  \end{center}
\end{figure}

Los datos sin procesar producidos por el plano de energía son formas de onda de PMTs, muestreadas cada \NewPMTSampling. El HPF asociado al esquema de cátodo conectado a tierra introduce una oscilación negativa en las formas de onda de los PMT. El primer paso en el procesamiento de datos es aplicar el algoritmo BLR descrito anteriormente a la línea de base arbitraria, formas de onda no procesadas y no calibradas, para producir formas de onda calibradas de línea de base, positivas y en cero. \Figu\ \ref{fig:rwf} muestra las formas de onda no procesadas correspondientes a los PMT del plano de energía, mientras que \figu\ \ref{fig:cwf} muestra las formas de onda calibradas correspondientes a la suma de los PMTs. El evento fue activado por la señal \textbf{EL-amplified} (\st) que aparece centrada en la ventana de adquisición de datos. La señal de señal de centelleo principal (\so) aparece al principio de la ventana de adquisición de datos.

%The deconvolution procedure permits full recovery of the energy of the waveforms. Indeed, the measured energy resolution for \Kr{83m} point-like energy deposits in \NEW\ at \NewSevenBarPressureRunII\ is \ResolutionKrFullFourSevenThreeFourWithSystematics\ (which extrapolates \SQRE\ to \ResolutionKrFullFourSevenThreeFourQbbWithSystematics) in the full chamber and
%\ResolutionKrFidFourSevenThreeFourWithSystematics\ (\ResolutionKrFidFourSevenThreeFourQbbWithSystematics) in a fiducial region
%($\R < \KrFidVolumeRRunII, \Z < \KrFidVolumeZRunII$) chosen to minimize the effect of lower solid angle coverage and large lifetime corrections \parencite{Martinez-Lema:2018ibw}.

El procedimiento de deconvolución permite la recuperación total de la energía de las formas de onda. De hecho, la resolución de energía medida para depósitos de\Kr{83m} de energía tipo punto en \NEW\ a \NewSevenBarPressureRunII\ es \ResolutionKrFullFourSevenThreeFourWithSystematics\ FWHM en el volumen completo. Una simple extrapolación de \SQRE\ a \Qbb\ produce \ResolutionKrFullFourSevenThreeFourQbbWithSystematics\ . El ajuste en la región fiducial definida como ($\R < \KrFidVolumeRRunII, \Z < \KrFidVolumeZRunII$). El corte radial garantiza una cobertura geométrica óptima y el corte z minimiza los errores residuales debidos a las fluctuaciones de la vida útil, que aumentan con z. El ajuste produce \ResolutionKrFidFourSevenThreeFourWithSystematics\, extrapolando a \ResolutionKrFidFourSevenThreeFourQbbWithSystematics en \Qbb . Este valor está razonablemente cerca de la mejor resolución esperada en \NEW\, lo que confirma las excelentes capacidades de la tecnología y las buenas condiciones de trabajo de la cámara. \parencite{Martinez:2018ibw}.


