
@echo off
cls
rem Uso:rem  genera_EPUB Nombre documento sin extesi�n "Autores separados por &" "T�tulo del libro"

echo *****************************************************
echo *                                                   *
echo    Vamos a generar el eBook: %1.epub               
echo *                                                   * 
echo *****************************************************
echo *                                                   * 
echo *   Pulse 1 para crear formulas mediante MathML     *
echo *   Pulse 2 para crear formulas mediante imagenes   *
echo *                                                   *
echo *****************************************************

CHOiCE /C 12

if %errorlevel%==1 goto :mathml
if %errorlevel%==2 goto :imagenes


:mathml

echo on

htlatex %1 "./EPUB_config.cfg,xhtml,mathml" "-cunihtf" "-cvalidate"

ebook-convert %1.html %1.epub --extra-css %1.css --cover EPUB_portada.png --level1-toc "//h:h2" --level2-toc "//h:h3" --page-breaks-before "//h:h2" --no-chapters-in-toc --book-producer "Universitat Polit�cnica de Val�ncia" --language "spanish" --authors %2 --title %3

goto :fin

:imagenes

echo on

htlatex %1 "./EPUB_config.cfg,html"

ebook-convert %1.html %1.epub --extra-css %1.css --cover EPUB_portada.png --level1-toc "//h:h2" --level2-toc "//h:h3" --page-breaks-before "//h:h2" --no-chapters-in-toc --book-producer "Universitat Polit�cnica de Val�ncia" --language "spanish" --authors %2 --title %3

:fin