\begin{savequote}[60mm]
"La ciencia, muchacho, está hecha de errores, pero de errores útiles de cometer, pues poco a poco, conducen a la verdad"
\qauthor{-- Julio Verne}
\end{savequote}

\chapter{Resultados de energía en NEW}\label{sec:Resultados}

\section{Primeros resultados sobre la resolución de energía del detector NEXT-White (NEW).}

Los primeros resultados de energía fueron obtenidos de mediciones usando fuentes de calibración de \Cs{137} y \Th{232} a una presión de 7,2 bar, durante la última parte del llamado Run II, que se extendió hasta otoño de 2017. Estas fuentes proporcionan eventos monoenergéticos a mayores energías y por lo tanto ofrecen pruebas estrictas de la reconstrucción de la energía en las condiciones similares a las señales de desintegración doble beta esperadas.

\begin{figure}[H]
	\centering
		\includegraphics[width=0.9\textwidth]{IMG/Chapter10/esquema_vessel_fuentes}
		\caption{Esquema del detector con la configuración de fuentes radioactivas en este estudio. La fuente de \Cs{137} esta situada en un puerto lateral de la vasija, mientras que la fuente de \Th{232} está situada encima de la vasija.\parencite{Renner:2018ttw}}
		\label{fig:vessel_fuentes}
\end{figure}

\begin{table*}
	\caption{Parámetros de operacion en NEW}
	\label{tab:fuentes}
	
	\begin{center}
		\begin{tabular}{| c | c |}
		\hline		
		Parámetros & Valor \\ \hline
		Presión & 7.2 bar \\
		E/p & $1.7 kV cm^{-1} bar^{-1}$ \\
		\textit{Drift field} & $400 V m^{-1}$ \\ 
		$V_{cátodo}$ & -28 kV \\
		$V_{puerta}$ & -7.0 kV \\
		Largo & 664.5 mm \\
		Diámetro & 454 mm \\
		\textit{EL gap} & 6 mm \\
		\textit{Drift length} & $(530.3 \pm 2.0) mm $\\ 
		Masa fiducial & 2.3 kg \\\hline
		\end{tabular}
	\end{center}
\end{table*}

Se han llevado a cabo estudios de linealidad y resolución en energía del detector NEW a alta energías utilizando tomas de datos (\textit{runs}) dedicados donde las fuentes radiactivas externas fueron empleadas como fuente de deposiciones de energía monoenergética, aunque espacialmente separadas. Se emplearon dos fuentes simultáneamente para este propósito: \Cs{137} y \Th{232}. La fuente de \Cs{137} se colocó en el puerto lateral de la vasija a presión, y la fuente \Th{232} se colocó en la parte superior de la vasija como se ilustra en la figura \ref{fig:vessel_fuentes}.

Estas fuentes proporcionan dos líneas de calibración bien definidas: el \Cs{137} se desintegra emitiendo un rayo gamma con energía de 661,6 keV y el \Th{232} decae eventualmente a \Tl{208} que emite un rayo gamma rayo con una energía de 2615 keV. La adquisición de datos fue activada por una señal \st en dos PMT integrando en un intervalo de 10-250 µs cuando ésta excede un umbral de activación especificado. El umbral de activación fue seleccionado para garantizar una eficiencia de detección completa para el \Cs{137} y deposiciones de mayor energía.

\subsection{Selección y reconstrución de eventos}

Estos resultados se enfocan en eventos producidos en la región del foto-pico de \Cs{137} (661 keV) y en la región del pico de “doble escape” de \Tl{208} (1592,5 keV). %Este último corresponde a eventos en el que la gamma de 2615 keV emitida en una desintegración de \Tl{208} convierte un campo eléctrico de un núcleo en un par electrón-positrón, y los dos rayos gamma de 511 keV producidos por la aniquilación de la escape de positrones no detectados.

Las interacciones de la radiación ionizante con xenón pueden ocasionalmente expulsar electrones de las capas internas de los átomos. Las subsiguientes transiciones de electrones a estas capas inferiores conducen a la emisión de rayos X característicos, varios de los cuales tienen energías cercanas a los 30 keV. Algunos de estos rayos x pueden viajar una distancia significativa de la traza de ionización principal antes de interactuar, o incluso escapar completamente el detector. Los rayos X que interactúan proporcionan una fuente adicional de energía casi monoenergética que se pueden utilizar para la calibración.

Las características de los eventos se calcularon a partir de las formas de onda de los PMT y SiPM adquiridas. Las señales \so y \st se identificaron como picos en la forma de onda de la suma de los PMTs (muestreadas en intervalos de 25 ns) en función de su ubicación y duración en el tiempo (ver \parencite{Martinez:2018ibw} para más detalles). Eventos con exactamente un pico \so  y al menos uno pero no más de tres picos de \st se aceptaron para análisis posteriores.

\subsection{Medida de energía}

La forma de onda de las señales PMT sumadas se integró en intervalos de 2 \micro s correspondientes al tiempo de tránsito de un electrón a través de la \textit{EL gap}. El patrón de las señales SiPM observadas en cada uno de los segmentos de tiempo fue utilizado para determinar el número y las posiciones (x, y) de las deposiciones de energía separadas, o \textit{Clusters}, presentes en el intervalo de tiempo dado. Si se reconstruyera más de un \textit{cluster} en un determinado
intervalo de tiempo, la energía detectada por los PMT se distribuiría proporcionalmente entre los \textit{clusters} según sus cargas, según lo determinado por las señales SiPM. Las coordenadas espaciales (x, y, z) de cada \textit{cluster} se utilizaron para determinar el factor de corrección de energía necesario por la falta de uniformidad de la respuesta en (x, y) y el tiempo de vida del electrón\footnote{Distancia recorrida por el electrón dentro de la cámara antes de ser absorbida por átomos remanentes en el gas xenón.}.

La energía total del evento se define como la suma de las energías de todos los \textit{clusters} reconstruidos en el evento. La distribución de energía resultante de los eventos reconstruidos en el volumen fiduciario completo se muestra en la Figura \ref{fig:energia_depositada}. El volumen fiduciario completo incluye la mayor parte del volumen activo del detector y se define como 50 mm \textless $Z_{min}$ , $Z_{max}$ \textless \hspace{2mm} 500 mm y $R_{max}$ \textless 180 mm donde $Z_{min}$ y $Z_{max}$ son las coordenadas z mínima y máxima, y $R_{max}$ es la coordenada radial máxima de todos los \textit{clusters} reconstruidos en un evento dado. Son visibles varios picos que serán analizados en más detalle más adelante, incluidos el fotopico \Cs{137}  y el pico de doble escape \Tl{208} $e^{+}e^{-}$ .

\begin{figure}[H]
	\centering
		\includegraphics[width=0.9\textwidth]{IMG/Chapter10/Energy_resolution}
		\caption{Distribución de energía de los eventos registrados en la región fiduciaria completa. El \Cs{137} (\textasciitilde 662 keV) y los fotopicos \Tl{208} (\textasciitilde 2,6 MeV) son visibles junto con sus espectros de Compton. Un pico también es visible a \textasciitilde 1,6 MeV debido a la producción de pares $e^{+}e^{-}$ a partir de la gamma de 2,6 MeV y el escape de ambas gammas 511 keV producidas en la posible aniquilación de positrones.}
		\label{fig:energia_depositada}
\end{figure}

\subsection{Distribución espacial de eventos y cortes fiduciales restringidos}

La Figura \ref{fig:energia_depositada} indica la presencia de las líneas de calibración de interés, aunque con \textit{background} significativo. La pureza de la muestra de calibración se puede mejorar aprovechando la localización espacial de las muestras de la fuente principal. Como se muestra en la Figura \ref{fig:distribuciones_energia}, la mayoría de los eventos en las lineas monoenergéticas están ubicadas cerca de las posiciones de origen. Para mejorar la relación señal-ruido de la muestra de calibración , se aplica un corte fiduciario, 160 mm \textless $Z_{min}$ , $Z_{max}$ \textless 300 mm, en el análisis posterior.

La respuesta del detector se degrada significativamente en los radios exteriores del volumen activo debido a recolección de luz imperfecta por parte de los PMT \parencite{Martinez:2018ibw}, lo que contribuye significativamente a la degradación de la resolución energética. Para eliminar el sesgo inducido por los eventos que se acercan a los bordes del detector
se aplica el corte fiduciario $R_{max}$ \textless 150 mm.

\begin{figure}[H]
	\centering
		\includegraphics[width=0.9\textwidth]{IMG/Chapter10/distribuciones_energia}
		\caption{Distribuciones de ubicaciones de eventos promedio en (x, y) (izquierda) y z (derecha), para eventos en la región del pico de rayos X de xenón $\in$ (28, 32) keV (arriba), el fotopico de \Cs{137}, $\in$ (650, 675) keV (centro), y eventos en la región del pico de doble escape, $\in$ (1550, 1640) keV (abajo). Las líneas rojas indican las regiones fiduciarias restringidas descritas.}
		\label{fig:distribuciones_energia}
\end{figure}

\subsection{Calibración de energía absoluta}

La energía de un evento $Q$, determinado integrando la señal \st medida, se expresa en
fotoelectrones utilizando factores de conversión determinados por la calibración de los PMT. Para determinar la escala de energía absoluta la respuesta medida del detector se comparó con los valores nominales de las energías del evento. Para ello se utilizaron tres muestras de eventos: el pico de rayos X de xenón de 29,7 keV, el
pico \Cs{137}, y el pico de doble escape. Las energías nominales fueron tomadas de \parencite{NuclearData} e incluyen el promedio ponderado de intensidad de las líneas de rayos X $K\alpha$ (29.669 keV), la energía del \Cs{137} emitido gamma (661,6 keV), y la energía del pico de doble escape (1592,5 keV).

\subsection{Resolución en energía}

A partir de una muestra de eventos monoenergéticos, la resolución de energía se puede determinar como la relación de la distribución FWHM de energía al valor medio de la respuesta. A bajas energías la energía
las deposiciones son casi puntuales y la resolución de energía está dominada por el término estocástico
reflejando las fluctuaciones en la producción de pares de iones de electrones. La medición para los eventos de altas energías implican una suma de las deposiciones de energía sobre el volumen más grande del detector. Estos eventos necesitan ser corregidos por la variación de la respuesta debido a la vida útil del electrón y/o las faltas de homogeneidad del detector y las imperfecciones residuales de las correcciones pueden contribuir a
la resolución energética (un error sistemático).

La relativa importancia de varias contribuciones puede ser estimada por comparación de la resolución de energía observada a diferentes energías: el término estocástico se espera que siga la dependencia $1/\sqrt{E} $ de la energía mientras que la contribución sistemática se espera que sea independiente de la energía. Por tanto, se espera que la variación de la resolución en energía siga:

\begin{equation}
R(E) = \sqrt{(\frac{a}{\sqrt{E}})^2+c^2}
\label{eq.res}
\end{equation}

O equivalentemente

\begin{equation}
R^2(E) = \frac{a^2}{E}+c^2
\label{eq.res1}
\end{equation}

donde $a/ \sqrt{E}$ es el término estocástico y c es el término sistemático y constante. Contribuciones potenciales de ruido, se comportan como b/E, se espera que sean despreciables en nuestro caso.

\subsection{Rayos X en Xenón}

La Figura \ref{fig:res_Xray} muestra la distribución de energía de los clústeres aislados de baja energía identificados dentro del volumen fiduciario. Se atribuyen dos picos visibles a varias líneas cerca de 29,7 keV y a varias líneas cerca de 34 keV. La distribución observada está bien descrita por una combinación de dos gaussianas y un polinomio de segundo grado. Como las líneas de rayos X más intensas en el pico de menor energía están mucho más juntas que en el pico de mayor energía, el ancho de la Gaussiana más a la izquierda, $(5,71 \pm 0,40) \% $FWHM, se toma para representar la resolución de energía a 29,7 keV. El error en la resolución es principalmente de origen sistemático y se estimó variando los rangos de binning y ajuste, variando ligeramente los cortes fiduciales, y considerando los errores estadísticos del ajuste, que fueron menores o iguales a la mitad del error declarado.

\begin{figure}[H]
	\centering
		\includegraphics[width=1\textwidth]{IMG/Chapter10/resultado_rayosX}
		\caption{(Izquierda) Ajuste de los picos de rayos X en xenón a dos gaussianas + un polinomio de segundo orden. (Derecha) La distribución resultante (x,y) de eventos incluidos en el ajuste.}
		\label{fig:res_Xray}
\end{figure}


\subsection{Gammas de alta energía}

Para determinar la resolución de energía del detector a energías más altas, la distribución de energía observada en
la vecindad del pico de 661,6 keV se ajustó como la suma de un polinomio gaussiano + un polinomio de segundo orden,
y, de manera similar, la distribución de la energía observada en la región del pico de doble escape se ajustó como la suma de una Gaussiana + exponencial (ver Figura \ref{fig:res_HE}). Los ajustes dan los resultados:
R(661,6 keV) = $(1,45 \pm 0,10)$ \% FWHM y R(1592,5 keV) = $(1,11 \pm 0,10)$ \% FWHM. Los errores
sobre la resolución se estimaron de la misma manera que para el caso de rayos X.

Al realizar un ajuste a la Ecuación \ref{eq.res1}, los valores medidos de la resolución de energía pueden ser
extrapolados a $Q_{\bb}$ dando como resultado $R(Q_{\bb})$ = $(1.02 \pm 0.09)$\% FWHM. El ajuste, que se muestra en la Figura \ref{fig:res_resolution}), da a = $(0.98 \pm 0.07)\% FWHM \cdot MeV$ e indica la presencia de un término constante significativo c = $(0.80 \pm 0.11)$\% FWHM que domina la resolución de energía en $Q_{\bb}$. Este término refleja el estado inicial del detector y es probable que esté relacionado con una vida útil del electrón relativamente pobre durante los \textit{runs} de calibración.

\begin{figure}[H]
	\centering
		\includegraphics[width=1\textwidth]{IMG/Chapter10/resultado_altas_energias}
		\caption{(Arriba a la izquierda) Ajuste del fotopico de \Cs{137} a un polinomio gaussiano + un polinomio de segundo orden en la región fiduciaria óptima seleccionada y (arriba a la derecha) la distribución resultante (x,y) de eventos incluidos en el ajuste. (Abajo a la izquierda) Ajuste del pico de doble escape de 1592,5 keV en la región fiduciaria óptimo seleccionada y (abajo a la derecha) la distribución resultante (x,y) de eventos en la región pico.}
		\label{fig:res_HE}
\end{figure}


\begin{figure}[H]
	\centering
		\includegraphics[width=1\textwidth]{IMG/Chapter10/res_resolution}
		\caption{$\%FWHM^{2}$ en función de 1/E. La línea roja representa un ajuste a la Ecuación \ref{eq.res1}, la línea vertical azul punteada indica la posición de $Q_{\bb}$.}
		\label{fig:res_resolution}
\end{figure}


%
%\section{Calibración de energía del detector NEXT-White con 1\% de resolución cerca de $Q_{\beta\beta}$ de \Xe{136} .}
%
%Los análisis anteriores de la resolución de energía NEXT-White utilizando rayos gamma de las fuentes de \Cs{137} y \Th{232} mostraron una resolución FWHM extrapolada del 1\% en $Q_{\beta\beta}$. La presión relativamente baja (7,2 bar) a la que se tomaron esos datos significaba que las trazas de electrones de los eventos con energía cercana a $Q_{\beta\beta}$ no se contuvieron fácilmente en el detector. La baja estadística cerca del fotopico de interés limitó la energía más alta del análisis detallado de la resolución de energía a 1,6 MeV. Después, se tomaron más datos a una presión más alta (10,3 bar), y los resultados se muestran en esta sección.
%
%\begin{figure}[H]
%	\centering
%		\includegraphics[width=1\textwidth]{IMG/Chapter10/res_2}
%		\caption{(Izquierda) Esquema de los componentes principales dela TPC NEXT-White y ubicaciones de las fuentes de calibración (no dibujadas a escala). Las fuentes \Cs{137} y \Th{228} se colocaron en los puertos de entrada lateral y superior de la vasija a presión, respectivamente, y una segunda fuente \Th{228} (la más a la izquierda de las dos) se colocó directamente encima de la vasija. (Derecha) Parámetros operativos utilizados en el estudio.}
%		\label{fig:res_2}
%\end{figure}
%
%\subsection{Configuración del run}
%
%Como el objetivo del presente análisis fue un estudio detallado de la resolución de energía, en la calibración se emplearon fuentes para producir picos de energía en un rango de varias decenas de keV incluyendo $Q_{\beta\beta}$. Se inyectó \Kr{83m} en el gas xenón, lo que proporcionó una distribución de deposiciones de energía puntuales de 41,5 keV utilizadas para mapear las variaciones en la geometría en las respuestas del sensor y la vida útil de los electrones del detector [13].
%
%\Cs{137} y las fuentes de calibración de \Th{228} también se colocaron como se muestra en la Figura \ref{fig:res_2}. La fuente de \Cs{137} proporcionó rayos gamma de 661,6 keV, y \Th{228} decae a \Tl{208} que proporciona rayos gamma de energía 2614,5 keV. En este estudio nos centramos en los picos de energía producidos por las interacciones de estos rayos gamma \Cs{137} y \Tl{208}, y también el pico de doble escape resultante de las interacciones de producción de pares $e+e-$ del rayo gamma \Tl{208} en el que los dos rayos gamma de 511 keV escapan. Para el presente análisis, el \textit{trigger} de adquisición se dividió en un \textit{trigger} de menor energía buscando los eventos de \Kr{83m} y un \textit{trigger} de alta energía destinado a capturar eventos con energía por encima de aproximadamente 150 keV.
%
%\subsection{Resolución en energía}
%
%Se seleccionaron pulsos individuales obtenidos del plano de energía sumados todos los PMT, (figura \ref{fig:S1_S2}) que fueron seleccionados y clasificados como S1 o S2. Se seleccionaron eventos con un único S1 identificado y los picos S2 se dividieron en trozos de 2 µs de ancho.
%
%\begin{figure}[H]
%	\centering
%		\includegraphics[width=1\textwidth]{IMG/Chapter10/S1_S2}
%		\caption{La forma de onda adquirida, suma de todos los PMT, para un evento en el fotopico de \Tl{208}. Hay que tener en cuenta que este evento en particular se identificó por contener un única traza continua, como es evidente parcialmente en la existencia de un solo pulso largo S2.}
%		\label{fig:S1_S2}
%\end{figure}
%
%El tiempo transcurrido desde que el pulso S1 fue utilizado para determinar la coordenada z de cada trozo de S2, y las energías E de las deposiciones reconstruidas (x,y,z,E) fueron entonces multiplicadas por dos factores de corrección: uno que representa la dependencia geométrica (x,y) de la colección de luz sobre el plano EL, y otro
%teniendo en cuenta las pérdidas debidas a la vida útil finita de los electrones. Este segundo factor dependía de la longitud de la deriva (coordenada z) y la ubicación en el plano EL (x,y), ya que la vida útil del electrón también varió en (x,y). Una vez completamente reconstruido, se hicieron cortes fiduciario en cada evento como se detalla en la Figura \ref{fig:res_distri}.
%
%\begin{figure}[H]
%	\centering
%		\includegraphics[width=1\textwidth]{IMG/Chapter10/res_distri}
%		\caption{Distribuciones normalizadas en $x-y$ (izquierda) y $z$ (derecha) de las deposiciones de energía observadas para eventos en el fotopico \Tl{208} de 2615 keV (datos del run 6485). Las líneas rojas continuas muestran los cortes fiduciarios empleados en este estudio que abarcan casi la totalidad del volumen activo . Hay que tener en cuenta, que los cortes fiduciales se colocan en todas las deposiciones de energía reconstruidas, rechazando un evento si una o más deposiciones ocurrieron fuera del corte.}
%		\label{fig:res_distri}
%\end{figure}
%
%Se aplicó una corrección final para una dependencia empíricamente observada en la orientación de la traza. El origen de esta dependencia, por el cual la energía medida de un evento disminuye con el aumento de la extensión axial (z) de la pista, todavía está en estudio. Sin embargo, nos encontramos que se puede corregir de manera efectiva de forma empírica.
%% de la siguiente manera. La extensión-z $\Delta z$ se define como la diferencia entre las coordenadas z máximas y mínimas de todos los cortes reconstruidos en el evento.  El efecto se muestra en la Figura \ref{fig:res_3} junto con la resolución obtenida para cada uno de los tres picos (662 keV, 1592 keV y 2615 keV) después de corregir el efecto usando el promedio de las pendientes normalizadas determinadas por un ajuste lineal a cada distribución,
%
%%\begin{equation}
%%E_{corrected} = \frac{E}{1-(m/b)\Delta z'}
%%\label{eq.E_corrrected}
%%\end{equation}
%
%%donde m y b son la pendiente y la intersección del ajuste lineal para $\Delta z$ en mm. Hay que tener en cuenta que se realizaron ajustes lineales en los eventos entre las líneas discontinuas. Variaciones razonables en el posicionamiento de estas líneas (reposicionándolas verticalmente por varios miles de SPEs sin cortes visibles en las áreas densas de las distribuciones 2D) dieron un error de aproximadamente $0.2 × 10^{-4}$ para cada calculado (m/b) además de los errores estadísticos que se muestran en las distribuciones en la Figura \ref{fig:res_3} (izquierda). Al determinar (m/b) y en la posterior determinación de la resolución de energía, se requirió que todos los eventos tuvieran longitudes z en los rangos mostrados en los ejes x de las distribuciones 2D. Además, para evitar complicaciones en el espectro causado por interacciones que producen deposiciones secundarias aisladas como
%%dispersión de Compton, bremsstrahlung y la emisión de rayos X característicos, en todos los eventos fue requerido haber sido reconstruido como traza continuas únicas.
%
%%\begin{figure}[H]
%%	\centering
%%		\includegraphics[width=1\textwidth]{IMG/Chapter10/res_3}
%%		\caption{Ajustes a la dependencia de la energía de la longitud de la traza en la dimensión axial 		(izquierda), y los espectros de energía resultantes de tres picos de energía (nominalmente a 662 keV, 1592 keV,
%%y 2615 keV) después de la aplicación de todas las correcciones, incluida una corrección lineal a la energía
%%(ecuación \ref{eq.E_corrrected}) correspondiente al valor promedio de  (m/b) = $2.76 × 10^{-4}$ obtenido de los 3 ajustes.}
%%		\label{fig:res_3}
%%\end{figure}
%
%Cada pico se ajustó a la suma de una Gaussiana y un polinomio de segundo orden para tener en cuenta la distribución circundante de eventos de \textit{background}, y la resolución se calculó utilizando el ancho de la Gaussiana. Las resoluciones obtenidas son: $1,20 \pm 0,02$\% FWHM a 662 keV; $0,98 \pm 0,03$ \% FWHM a 1592 keV; y $0,91 \pm 0,12$\% FWHM a 2615 keV. Los errores totales se estiman en cada caso en base a los errores estadísticos de los ajustes (mostrado en la histogramas en la Figura \ref{fig:res_3}) y efectos sistemáticos que incluyen variaciones en el rango de eventos incluidos en el ajuste y errores (sistemáticos) en la corrección por el efecto de la longitud axial. La conversión de energía de los fotoelectrones detectados a keV fue determinada (después de la aplicación
%de todas las correcciones) utilizando un ajuste cuadrático a las medias de los tres picos de interés (662 keV,
%1592 keV, 2615 keV) y el pico de rayos X de xenón $K-\alpha$ de 29,7 keV. Los rayos X tenían energías demasiado bajas para ser activada como eventos individuales, pero sus energías eran visibles al examinar el espectro de deposiciones de energía aisladas dentro de todos los eventos, que incluyeron pequeñas deposiciones debido a los rayos X de xenón que se alejaron de la traza principal antes de interactuar.
%
%
%\begin{figure}[H]
%	\centering
%		\includegraphics[width=1\textwidth]{IMG/Chapter10/res_4}
%		\caption{Espectro de energía completo para eventos con energías superiores a aproximadamente 150 keV. Se aplicaron correcciones para la vida útil de los electrones y los efectos geométricos a todos los eventos, así como una corrección por el efecto de longitud axial descrito correspondiente a (m/b) = $2,76 \times 10^{-4}$. Además de las tres líneas examinadas en detalle en este estudio, las líneas también están presentes debido a otros rayos gamma de la cadena de desintegración \Th{228}: a 238 keV (de desintegración \Pb{212} $\rightarrow$ \Bi{212}), 511 keV (aniquilación $e^{+}e^{-}$, con alguna contribución del decaimiento \Tl{208} $\rightarrow$ \Pb{208}), 583 keV (\Tl{208} $\rightarrow$ decaimiento de \Pb{208}), 727 keV (decaimiento de \Bi{212} $\rightarrow$ \Po{212}) y 860 keV (decaimiento de \Tl{208} $\rightarrow$ \Pb{208}).
%		}
%		\label{fig:res_4}
%\end{figure}
%
%El espectro de energía de los \textit{triggers} de alta energía en el volumen activo completo se muestra en la Figura \ref{fig:res_4} después de aplicar todas las correcciones descritas en esta sección.
%
%A diferencia del estudio anterior [12], el fotopico de \Tl{208} a 2615 keV (cerca de $Q_{\beta\beta}$) está claramente resuelto. La resolución cuadrática se muestra frente a la energía inversa en la Figura \ref{fig:res_5} para los tres picos de energía estudiados y ajuste a una recta, $R^{2} = a/E + c$, donde a = $548,52 \pm 82,15 \%FWHM^{2} \cdot keV $ y c = $0,62 \pm 0,10 \%FWHM^{2}$. La presencia de un término constante en la resolución muestra que los efectos sistemáticos del detector específico, no han sido completamente eliminados en nuestro análisis, y todavía hay margen para seguir mejorando. Sin embargo, estos resultados demuestran que una excelente resolución en energía 
%se puede obtener a lo largo de todo el volumen fiduciario una vez que se hace la corrección para el efecto de la  longitud axial. 
%
%A diferencia de las correcciones por el tiempo de vida de los electrones, la corrección por el efecto de la longitud axial fue relativamente estable a lo largo del tiempo (alrededor de 8 semanas) durante el cual se tomaron los datos presentados en este estudio. Se cree que este efecto es el resultado de cierta no linealidad interna
%en la producción de luz, posiblemente debido a alteraciones del campo \textbf{EL} durante la lectura de la traza en un efecto de \textquotedbl \textit{charging-up} \textquotedbl cuando los electrones cruzan el espacio \textbf{EL}, o debido a la pérdida de electrones en el espacio \textbf{EL} del apego a las impurezas producidas al fotoionizar el material que cambia la longitud de onda depositado en la superficie detrás de la región \textbf{EL}.
%
%\begin{figure}[H]
%	\centering
%		\includegraphics[width=1\textwidth]{IMG/Chapter10/res_5}
%		\caption{Resolución de energía al cuadrado representada frente a 1/E. Los puntos medidos fueron ajustados a la forma funcional $R^{2} = a/E + c$, con a = $548,52 |pm 82,15 \%FWHM^{2} \cdot keV$ y c = $0,62 \pm 0,10 \%FWHM^{2}$.}
%		\label{fig:res_5}
%\end{figure}


