\begin{savequote}[60mm]
"La ciencia, muchacho, está hecha de errores, pero de errores útiles de cometer, pues poco a poco, conducen a la verdad."
\qauthor{-- Julio Verne}
\end{savequote}

% -------------------------------------------------
%	Bases de los PMTs
% -------------------------------------------------


\chapter{Bases de polarización}\label{sec:bases}

Debido al tamaño y complejidad del plano de energía, el diseño del circuito de la base debe ser lo más simple posible. Como consecuencia, la topología pasiva inicial se ha mantenido en un primer enfoque. Sin embargo, el tamaño y la cantidad de componentes deben estudiarse cuidadosamente para lograr una buena linealidad. También se ha identificado un importante compromiso entre la radiopureza y la linealidad. 

El diseño se ha optimizado para preservar la linealidad en todo el rango dinámico de 0.1 fotones/ns a 1 fotones/ns para pulsos de luz más largos que \SI{150}{\micro\second}. Esta es una característica crucial, en particular para medir la energía de los electrones de alta energía que dan como resultado largas trazas en el detector (y, por lo tanto, en señales tan largas como \SI{150}{\micro\second}). Para hacer frente a esas largas señales, el circuito de la base debe proporcionar una carga suficiente para el PMT con un cambio insignificante en la tensión de dinodo a dinodo para una corriente promedio máxima de 0.1 mA. Los cambios en estas tensiones introducen una ganancia variable en el tiempo que resulta en un mecanismo de distorsión no lineal que tiene un fuerte efecto en la linealidad del PMT.


\section{Requerimientos de diseño}

Uno de los componentes electrónicos con más radioctividad son los condensadores que utilizan principalmente sustratos a base de cerámica. Por lo tanto, el objetivo principal de la propuesta se centrará en reducir el número y el tamaño de los condensadores a un mínimo que cumpla con la especificación de linealidad sin comprometer el nivel de radiopureza del plano de energía.

Las valores de la cadena de resistencias han sido elegidos basándonos en las recomendadas por el fabricante del PMT según el ratio, y el valor final de las resistencias se ha ajustado según las restricciones térmicas. Los PMTs se mantienen a vacío, por lo que el circuito de la base debe disipar la potencia generada por los componentes electrónicos, para ello se decidió recubrir de epoxy térmica radiopura con una alta capacidad de disipación de potencia y un alto valor dieléctrico.

Según el valor de resistencia elegidos, la potencia total disipada por la base de polarización es la siguiente:

\begin{equation}
	P=\frac{V^2}{R} = \frac{1230^2}{35M} = 43 mW
\end{equation}


 Una potencia total de $\backsim$\SI{40}{\milli\watt} en estas condiciones produce una temperatura estable de $30\degree$ que no introduce alteraciones en el resto del detector. Las simulaciones y test para demostrarlo serán explicados en las secciones \ref{cap:simu} y \ref{cap:test_t}.

El valor de los condensadores del diseño se calculó en función del algoritmo que se muestra en la \figu\ \ref{fig:Base_Sizing}. El objetivo principal es que la señal máxima en términos de fotones introduce una caída de tensión por debajo del 0.1\% de la tensión entre los dínodos. Dado que cada etapa aumenta la ganancia en un factor dado (en función de la geometría y las características del PMT), los casos más desfavorables para esta caída de tensión se ubicarán en las últimas etapas donde la cantidad de carga que entregará el PMT es mayor. Por lo tanto, a partir de la última etapa de PMT, se respaldará un cierto número de etapas con condensadores.

\begin{figure*}[!htbp]
  \begin{center}
    \includegraphics[width=1.1\textwidth, angle=0]{IMG/Chapter3/Base_Sizing.pdf}
    \caption{Esquema de la base. Procedimiento dimensionamiento de los condensadores}
    \label{fig:Base_Sizing}
  \end{center}
\end{figure*}

Para cumplir con los límites de radiopureza, se probó una primera propuesta basada en una cobertura mínima de 2 etapas. Debido a que los componentes deben tener la mínima radioactividad posible, los condensadores polarizados de tántalo fueron los elegidos, estos condensadores tienen una alta capacidad nominal y son los más radiopuros que se han encontrado en el mercado. Esta decisión fue basada en los datos de las bases de datos de radiopureza, además de las medidas realizadas en Canfranc por el personal del \textit{LSC} con detectores de germanio, de diferentes tipos de condensadores.

Se utilizaron grupos de condensadores conectados en serie para resistir la alta tensión necesaria entre los dínodos, ya que los condensadores de tántalo tienen un voltaje máximo más bajo que el valor que se necesita tener entre dínodos. 

Finalmente se probó con 5 condensadores (2 en la última etapa y 3 en la penúltima) para soportar la alta tensión entre terminales (\figu\ \ref{fig:Base_5C_sch}). Los resultados se muestran en la \figu\ \ref{base:Base_5C}. Aunque los gráficos de linealidad muestran una respuesta bastante lineal, hay algunas longitudes de pulso que fallan en el ajuste como se muestra en \figu\ \ref{fig:Base_5C_lin}. La señal de salida no muestra un efecto de sobrecarga típico que aparecería debido a una caída usual en el valor de ganancia para un alto número de fotones. Este comportamiento está relacionado con un mecanismo de distorsión que aparece debido a una redistribución de carga en los condensadores de la base. Cuando un condensador se agota, drena la carga del anterior. Cuando la carga drenada por el último condensador es demasiado alta, la caída de tensión en la etapa anterior afecta la ganancia general. La longitud del pulso de señal capaz de generar esta distorsión depende de las constantes de tiempo de cada etapa y puede cambiar con la distribución de voltaje entre etapas. La linealidad medida fue 1.95 \% para 100k pe. 

\begin{figure}[!htbp]
  \begin{center}
    \includegraphics[width=1.1\textwidth]{IMG/Chapter3/5C_base_d}
    \caption{Esquema de una base de PMT con condensadores en las 2 últimas etapas}
    \label{fig:Base_5C_sch}
  \end{center}
\end{figure}


\begin{figure}
 \centering
  \subfloat[Signal]{
  \label{fig:Base_5C}
     \includegraphics[width=0.35\textwidth]{IMG/Chapter3/5C_base_b}}
    \hspace{2mm}
  \subfloat[Linearity]{
    \label{fig:Base_5C_lin}
    \includegraphics[width=0.4\textwidth]{IMG/Chapter3/5C_lin.pdf}}
 \caption{PMT base with 2 stage coverage}
 \label{base:Base_5C}
\end{figure}

%\vspace{35mm}

Observando los resultados, esto significa que se requiere una mejor cobertura de condensadores, por lo que se propuso una cobertura de 3 etapas (\figu\ \ref{fig:Base_7C_sch}). Este circuito prácticamente no muestra distorsión del tiempo. El ajuste de linealidad obtenido fue de 0.38\% para 140k pe. Lo que excede los requisitos iniciales de linealidad \figu\ \ref{base:Base_7C}. Los estudios completos de linealidad y el desarrollo del test se muestran en la sección \ref{cap:lin}.


\begin{figure}[!htbp]
  \begin{center}
    \includegraphics[width=1.1\textwidth]{IMG/Chapter3/7C_base_a}
    \caption{Esquema de una base de PMT con condensadores en las 3 últimas etapas}
    \label{fig:Base_7C_sch}
  \end{center}
\end{figure}


\begin{figure}
 \centering
  \subfloat[Signal]{
  \label{fig:Base_7C}
     \includegraphics[width=0.35\textwidth]{IMG/Chapter3/7C_base_b}}
    \hspace{2mm}
  \subfloat[Linearity]{
    \label{fig:Base_7C_lin}
    \includegraphics[width=0.4\textwidth]{IMG/Chapter3/7C_lin.pdf}}
 \caption{PMT base with 2 stage coverage}
 \label{base:Base_7C}
\end{figure}


\section{Diseño}

La base PMT es un circuito pasivo que incluye 19 resistencias de diferentes valores de resistencia eléctrica, 7 condensadores de tántalo (3 con una capacidad de \SI{1.5}{\micro\farad} y 4 con \SI{4.7}{\micro\farad}), 18 pines soldados en una tarjeta de circuito electrónico de kapton (\fig\ \ref{fig:7C}) recubierto con epoxy \parencite{Araldite2011} para evitar la ruptura dieléctrica en vacío moderado o en una atmósfera de \NIT, y para mejorar el contacto térmico con la tapa de cobre (\figu\ \ref{base:Base_7C_draw}). Los parámetros críticos para el circuito base de PMT son la tensión de operación (\NewPMTOperatingVoltage) y la ganancia de PMT correspondiente ($2.5 \times 10^6$).

La tensión aplicada al PMT se ha establecido a través de un equilibrio entre la capacidad de detección de fotones individuales y el rango de alta tension recomendado por el fabricante para una mejor linealidad. Para lograr una relación señal-ruido (SNR) suficiente, el ruido del FEE debe reducirse, lo que limita la ganancia máxima del FEE. Por lo que requiere una ganancia del PMT más alta. Un banco de pruebas para mediciones de linealidad se desarrolló a medida para optimizar el diseño de la base del PMT y probar los PMTs del plano de energía bajo las condiciones del plano de energía de \NEW\ . 

\begin{figure*}[!htbp]
\centering
  \includegraphics[width=\textwidth]{IMG/Chapter3/base_sch.png} \\
\caption{\NEW\ PMT base circuit.}
\label{fig:7C}
\end{figure*}

%\begin{figure}
%	\centering
%	\includegraphics[width=0.4\textwidth]{IMG/Chapter3/7C_layout.pdf}
%	\caption{Base layout.} 
%	\label{fig:7C_layout}
%\end{figure}

\begin{figure}
 \centering
  \subfloat[Base with the copper cap and filled of epoxy]{
  \label{fig:7C_base}
     \includegraphics[width=0.4\textwidth]{IMG/Chapter3/Base_1}}
    \hspace{2mm}
  \subfloat[Scheme of the PMT base.]{
  \label{fig:7C_base_sch}
    \includegraphics[width=0.5\textwidth]{IMG/Chapter3/scheme_base}}
 \caption{Base}
 \label{base:Base_7C_draw}
\end{figure}

Finalmente, la radioactividad del circuito de la base del PMT fue medida siendo cuatro veces más grande que la del propio PMT, como se muestra, para la actividad del \BI\ en la tabla \ref{tab.PMTBudget} (consulte \parencite{Cebrian:2017jzb} para obtener una descripción detallada). Sin embargo, hay que tener en cuenta que la radioactividad introducida en \NEW\ por la base está parcialmente protegida por el plato de cobre, y contribuye aproximadamente al mismo recuento de \textit{background} final que los PMT.

\begin{table}[!h]
\caption{Radioactive \BI\ budget of \NEW\ PMTs and base circuits.}
\begin{center}
\begin{tabular}{|c|c|}
\hline
Componente & \BI\ actividad por unidad\\
\hline\hline
PMT &  \SI{0.35}{\milli\becquerel}\\
\hline
Base & \\ 
Condensadores 1.5 uF & \SI{72}{\micro\becquerel} \\
Condensadores 4.7 uF & \SI{123}{\micro\becquerel}\\
Resistencias Finechem & \SI{4.1}{\micro\becquerel}\\
Resistencias KOA RS & \SI{7.7}{\micro\becquerel}\\
Pines & \SI{1.1}{\micro\becquerel} \\
Araldite epoxy & \NewBaseEpoxy\\
cable Kapton-Cu & \NewBaseCable\\
Sustrato de Kapton & \SI{23}{\micro\becquerel}\\
tapa "cap" de cobre & \SI{12}{\micro\becquerel}\\
\hline
Total base  & \SI{1.2}{\milli\becquerel} \\ 
\hline
\end{tabular}
\end{center}
\label{tab.PMTBudget}
\end{table}%

\section{Simulaciones térmicas}\label{cap:simu}

Tal y como se ha mencionado anteriormente, tanto los PMTs como las bases deben de poder trabajar en un vacío moderado y para ello debíamos asegurarnos la mejor distribución para conseguir una buena disipación térmica. 

Se simuló la base con la cobertura de cobre, la cual llamamos \textit{cap} de cobre,  y sin él, con ello se pretendía conocer la temperatura alcanzada por la base en las diferentes situaciones y saber que era necesario para conseguir la mejor disipación. En la \figu\ \ref{fig:simu} se puede ver una simulación en la que la base no lleva \textit{cap} de cobre, por tanto la disipación se haría simplemente por una trenza de cobre desde la PCB. En la figura se observa que la temperatura alcanza los $120\degree$, siendo totalmente inviable no poner el \textit{cap} de cobre como se comprobó con la simulación \figu\ \ref{base:simu}.

\begin{figure*}[!htbp]
\centering
  \includegraphics[width=0.4\textwidth]{IMG/Chapter3/simu_comsol} \\
\caption{Simulación térmica de la base sin \textbf{cap} de cobre en Comsol multiphisics.}
\label{fig:simu}
\end{figure*}

Tras descartar no poner una parte de cobre para conseguir disipar lo máximo, se realizó simulación con el \textit{cap} de cobre como se muestra en la \figu\ \ref{base:simu}, donde la trenza de cobre sale del \textit{cap} de cobre y según la simulación con esta distribución de materiales, la base alcanza $28\degree$.

\begin{figure}[!h]
 \centering
  \subfloat[Simulación térmica de la base con \textit{cap} de cobre en Comsol multiphisics (Vista lateral).]{
  \label{fig:simu}
     \includegraphics[width=0.4\textwidth]{IMG/Chapter3/simu_comsol_1}}
    \hspace{2mm}
  \subfloat[Simulación térmica de la base con \textit{cap} de cobre en Comsol multiphisics (Vista general).]{
  \label{fig:simu2}
    \includegraphics[width=0.35\textwidth]{IMG/Chapter3/simu_comsol_2}}
 \caption{Simulación térmica de la base en Comsol multiphisics.}
 \label{base:simu}
\end{figure}

En todas las simulaciones se puede ver una trenza de cobre que es la que se encarga de transmitir el calor desde la base hasta el plano de cobre, lo que intentábamos descubrir con las simulaciones era conocer si era necesario cubrir la base con el \textit{cap} de cobre, y descubrimos que era totalmente necesario. La trenza de cobre finalmente se sujeto al centro de la base a través de una arandela y tuerca sujetada a la varilla roscada que pusimos en la misma base (\figu\ \ref{base:Base_7C_draw}) . 

\section{Tests}

\subsection{Test de tensión}

Un test totalmente necesario aunque a su vez sencillo, es comprobar que la base es alimentada con la alta tensión necesaria sin que se produzcan ningún tipo de imprevisto ni daño, incluyendo chispas que pudieran dañar el circuito. En este test se hicieron varias comprobaciones. 

El primer test básico se realizó en una caja negra, conectando la cadena completa, es decir, se conectó la base con su cable de Kapton a un pasamuros de los que después se instalaría en \NEW\ , desde el pasamuros hasta el \textit{FEE} se utilizó el mismo cable con la misma distancia que se instaló en NEW. Desde la fuente de alta tensión hasta el \textit{FEE} se utilizó un cable coaxial RG58 con conectores SHV en ambos extremos del cable (\figu\ \ref{fig:test_HV}). En un primer test no se conectó el \textit{FEE} al sistema de adquisición para evitar posibles daños en el sistema de adquisición en caso de que hubiera fallado el funcionamiento de la base y se hubiera producido algún corto o una chispa. 

\begin{figure}[!h]
	\begin{center}
		\includegraphics[width=0.9\textwidth]{IMG/Chapter3/Setup_HV}
		\caption{Banco de pruebas para los tests de alta tensión}
		\label{fig:test_HV}
	\end{center}
\end{figure}

Para esta primera parte del test se aplicaron a la base 1.700V, sabiendo que en su funcionamiento normal el PMT está a una tensión nominal de 1.230V y que la tensión máxima que recomienda el fabricante para el fotomultiplicador es 1.750V, se aplicó aproximadamente un 35\% de margen para asegurarnos que el funcionamiento después fuera el correcto, sin riesgos en caso de tener que aplicar más tensión a la base para cambiar la ganancia del PMT. La fuente de alta tensión estaba conectada en todo momento al \textit{slow control} donde se monitorizaba y almacenaba la información de la tensión y la corriente. En caso de un consumo de corriente excesivo, que superara un umbral programado, tanto en la fuente como en el programa, este apagaba automáticamente la fuente de alta tensión para no dañar ninguna parte del sistema.
 Este test se realizó durante varios días dejando el la base a alta tensión, comprobando que el consumo de corriente de la base era el correcto y no habían variaciones inesperadas de tensión ni corriente en ningún momento. 

Tras comprobar con el primer test que nada parecía dañar el sistema, se conectó el \textit{FEE} al sistema de adquisición mediante un cable HDMI, de este modo lo que se quería observar era si se producían micro-chispas que produjeran pequeños picos, tan pequeños que no se observaran cambios de corriente pero los suficiente grandes que parecieran SPE sin serlos, engañando de esa manera en la toma de datos. Con este test se terminó de validar la base desde el punto de vista de la alta tensión.

%\begin{figure}[H]
%	\begin{center}
%		\includegraphics[width=0.5\textwidth]{IMG/Chapter3/404}
%		\caption{Imagen del test}
%		\label{fig:test_HV2}
%	\end{center}
%\end{figure}

\subsection{Test térmicos}\label{cap:test_t}

Para comprobar realmente la temperatura que alcanzaría la base se realizaron diferentes tests donde se midió la temperatura obtenida, comprobando la veracidad de las simulaciones en Comsol multiphisics (\figu\ \ref{fig:test_temp}).

\vspace{10mm}

Los elementos utilizados para realizar el test fueron:

\begin{itemize}

\item Base (Circuito de Kapton\textregistered + epoxy + \textit{cap} de cobre) 
\item Sensor 1-wire\textregistered: Es un sensor digital de temperatura .
\item Malla de cobre: Es para conectar el \textit{cap} de cobre de la base con la vasija para disipar el calor.
\item Vasija: Es una cámara cilíndrica de acero inoxidable AISI304L con tapas finales DN100CF y 300 mm de longitud. 
\item Pasamuros: 1 DN100CF de la compañia Allectra\textregistered\  para los conectores de la base y 1 DN40CF para el sensor de temperatura.
\item RGA: Pfeiffer\textregistered\ PrismaPlus QMC220 M2
\item Bomba de vacío: Pfeiffer\textregistered\ HiCube
\item Fuente de alimentación de alta tensión: Caen\textregistered\ N1470
\end{itemize}

Lo primero que se realizó fue conectar el cable de la base por el cual la tensión es aplicada y cerrar la vasija por el lado con pasamuros. Después se procedió a hacer la misma operación con el cable del sensor de temperatura y la malla de la base, tras conectar los cables se procedió a cerrar el lado opuesto de la vasija. Una vez la vasija estuvo cerrada, se conectó a través de una conexión VCR la bomba de vacío y el espectrometro de masas (\textit{RGA}). El siguiente paso fue obtener un vacío suficiente para asegurarnos de no tener chispas y trabajar de manera segura con alta tensión, después se realizó un test de fugas en la vasija ayudándonos de una bombona de gas Argón y el \textit{RGA}. El Argón fue aplicado sobre las juntas con una pistola y comprobado en el \textit{RGA} que no hubiese fugas en el sistema.

En la imagen \figu\ \ref{fig:test_temp} se puede observar una cobertura de Teflón en la base, es una pieza de soporte que se decidió utilizar en la prueba para evitar de esa manera que la base tocara la vasija, lo que hubiera falseado la medida del test.

\begin{figure}[!h]
	\begin{center}
		\includegraphics[width=0.4\textwidth]{IMG/Chapter3/test_temp}
		\caption{Vista del interior de la vasija donde se realizaron los test de temperatura de la base, antes de conectar el cable del sensor de temperatura y la trenza metálica.}
		\label{fig:test_temp}
	\end{center}
\end{figure}

\vspace{5mm}

Por otra parte, el sensor de temperatura estaba conectado a un PC a través de una tarjeta electrónica suministrada por el propio fabricante \textit{DALLAS semiconductors} (\figu\ \ref{fig:test_controlador}) junto a su correspondiente driver. Sobre el sensor de temperatura decir que utiliza el protocolo de comunicaciones 1-wire\textregistered\ , un protocolo en serie diseñado por la misma compañía. Dicho protocolo esta basado en un bus maestro y varios esclavos que se conectan a través de una sola línea de datos por la que además se alimenta el sensor. Dado que en el laboratorio disponíamos del sensor, la tarjeta controladora y el programa de PC, se decidió utilizar este sistema para realizar el test. 


\begin{figure}[h!]
 \centering
  \subfloat[Controlador suministrado por la compañia Dallas semiconductors.]{
  \label{fig:test_controlador}
     \includegraphics[width=0.5\textwidth]{IMG/Chapter3/maxim.jpg}}
    \hspace{2mm}
  \subfloat[Detalle del sensor de temperatura conectado a la base.]{
    \label{fig:sens}
    \includegraphics[width=0.42\textwidth]{IMG/Chapter3/detalle_sens}}
    \caption{Sensor de temperatura 1-wire y su controlador}
\end{figure}


Una vez la vasija estuvo preparada, se suministró la tensión a la base con la fuente de alimentación y se verificó su funcionamiento y la temperatura durante 1 semana, obteniendo de media $26\degree$. En las figuras \figu\ \ref{fig:tab_test} y \figu\ \ref{fig:gra_test} se puede apreciar que la temperatura se mantiene estable entre $25\degree$ y $27\degree$ durante una semana.  

\begin{figure}[H]
	\begin{center}
		\includegraphics[width=0.5\textwidth]{IMG/Chapter3/404}
		\caption{Tabla con los datos del test.}
		\label{fig:tab_test}
	\end{center}
\end{figure}

\begin{figure}[H]
	\begin{center}
		\includegraphics[width=1\textwidth]{IMG/Chapter3/graph_temp}
		\caption{Gráfica con los datos del test.}
		\label{fig:gra_test}
	\end{center}
\end{figure}


\subsection{Test de linealidad}\label{cap:lin}

Para el test de linealidad se completó el banco de pruebas que se utilizó para los test de tensión, a este se le añadió un PMT y un LED. Dado que la cantidad de luz producida por el LED en condiciones de polarización estables era demasiado alta para una exposición directa al PMT, se utilizó un conjunto de filtros ópticos de atenuación del 90\% acoplados directamente al LED. Con el fin de reducir el efecto de las fluctuaciones del LED, cada medición se promedió con 200 muestras. Era importante tener el sistema completo ya que el FEE asociado tiene características especiales para las señales capturadas y la linealidad requerida. El rango dinámico de cada canal del plano de energía debía cubrir desde un solo fotón hasta $100 \times 10^3$ fotones. Además, la variabilidad de la longitud de la señal es una especificación desafiante, ya que el PMT debía soportar señales de unos pocos \micro s hasta 150 $\micro$s.

\begin{figure}[!h]
	\begin{center}
		\includegraphics[width=0.9\textwidth]{IMG/Chapter3/Setup_linealidad}
		\caption{Visión general del banco de pruebas para mediciones de linealidad}
		\label{fig:LED_testbench}
	\end{center}
\end{figure}

El banco de pruebas de linealidad hace uso de un generador de señales que introduce un pulso cuadrado de tensión con tiempos de subida y bajada muy cortos en un LED de 435 nm. El número de fotones generados es proporcional a la longitud del pulso. %Para evitar los efectos de conmutación del LED, la duración mínima del pulso es de 30 \micro s (el tiempo de conmutación del LED es del orden de 10 ns). 
 Se tomaron diferentes tandas de datos con la misma amplitud de pulso pero diferentes anchuras desde 1 \micro s hasta 50 \micro s en bloques de 5 \micro s y desde 50 \micro s hasta 300 \micro s en bloques de 50 \micro s.

Con el fin de poder comprobar la linealidad de dos bases diferentes, mismo diseño pero pero menos cantidad de condensadores de desacoplo, en el mismo banco de pruebas se colocaron 2 bases de polarización, una de 5 condensadores y una con 7 condensadores (\figu\ \ref{fig:LED_testbench}). En frente de los PMTs un único LED para poder tomar datos a la vez y comparar los resultados. Esta prueba se realizó, desde el punto de vista de la radiopureza, para averigurar el número mínimo de condensadores necesarios para asegurar linealidad en el rango de fotones que se esperaba tener.

Para poder obtener valores de linealidad correctos, primero se obtuvo el valor de un SPE en cada uno de los PMTs, ya que cada PMT ante una misma tensión de alimentación puede tener ganancia ligeramente superior o inferior. Para estos tests, primero se buscó con el osciloscopio la configuración mínima para poder ver fotones sueltos y de esa manera obtener el valor del SPE. En las figuras \ref{fig:SPE_5C} y \ref{fig:SPE_7C} se muestran los histogramas de donde se obtienen los valores de SPE de cada PMT.

\begin{figure}[h!]
 \centering
  \subfloat[Histograma de la base de 5 condensadores. SPE 30 cuentas]{
  \label{fig:SPE_5C}
     \includegraphics[width=0.4\textwidth]{IMG/Chapter3/SPE_5C}}
    \hspace{2mm}
  \subfloat[Histograma de la base de 7 condensadores. SPE 38 cuentas]{
    \label{fig:SPE_7C}
    \includegraphics[width=0.4\textwidth]{IMG/Chapter3/SPE_7C}}
\end{figure}

Una vez conocido el valor del SPE se procedió a obtener las gráficas de linealidad. Se realizaron varias gráficas, una con valores de pulsos entre 1\micro s y 50\micro s y otra entre 50\micro s y 200\micro s o 300\micro s para representar aproximadamente la misma cantidad de fotones. En las siguientes figuras se muestran las gráficas de linealidad.

\begin{figure}[h!]
 \centering
  \subfloat[Gráfica de linealidad de la base con 5 condensadores. 1\micro s a 50\micro s]{
  \label{fig:5C_1}
     \includegraphics[width=0.4\textwidth]{IMG/Chapter3/Linealidad_5C_1_a_50}}
    \hspace{2mm}
  \subfloat[Gráfica de linealidad de la base con 7 condensadores. 1\micro s a 50\micro s]{
    \label{fig:7C_1}
    \includegraphics[width=0.4\textwidth]{IMG/Chapter3/Linealidad_7C_1_a_50}}
\end{figure}

\begin{figure}[h!]
 \centering
  \subfloat[Gráfica de linealidad de la base con 5 condensadores. 100\micro s a 300\micro s]{
  \label{fig:5C_50}
     \includegraphics[width=0.4\textwidth]{IMG/Chapter3/Linealidad_5C_50_a_300}}
    \hspace{2mm}
  \subfloat[Gráfica de linealidad de la base con 7 condensadores. 50\micro s a 300\micro s]{
    \label{fig:7C_50}
    \includegraphics[width=0.4\textwidth]{IMG/Chapter3/Linealidad_7C_50_a_300}}
\end{figure}


De las gráficas se puede extraer que para pocos fotones ambas bases son bastante lineales, pero si esperamos más de 160k fotones necesitamos la base de 7 condensadores.

Para concluir si la linealidad de la base era lo suficientemente buena nos basamos en los valores obtenidos para el $\chi^2$ (chi cuadrado) y para el $\chi^2 reducido$ como se detalla a continuación:

\begin{equation}
\chi^2 = \sum_{i=1}^{length}\left(\frac{(Y - f(x))}{\sigma}\right)^2
\end{equation}

\begin{equation}
\chi^2 reducido = \frac{\chi^2}{length(x) - 2}
\end{equation}

Por ejemplo, para la \figu\ \ref{fig:5C_1} quedaría:

\begin{equation}
\chi^2 = \sum_{i=1}^{11}\left(\frac{(Y - f(x))}{\sigma}\right)^2 = 6.144
\end{equation}

\begin{equation}
\chi^2 reducido = \frac{\chi^2}{11 - 2} = 0.683
\end{equation}

\subsection{Test de \textquotedbl chispas \textquotedbl  en vacío (Pashen)}

El objetivo de este test era conseguir averiguar el nivel mínimo de vacío necesario para que el sistema pueda trabajar en un regimen estable, y no tener cortocircuitos en ningún lugar dentro de la electrónica del plano de energía por culpa de un mal vacío. El test se realizó teniendo control y monitorización continuo con 2 sensores de vacío, y a través del \textit{slow control}.

Durante todo momento se observó, por una parte la base de polarización (\figu\ \ref{base:pashen}) de manera que se pudiera observar una posible chispa producida por un cortocircuito, y por otra parte se fue mirando los 2 sensores de vacío, uno en el propio plano de energía y otro en la bomba de vacío. Para detectar la posible chispa de manera visual, además de a través de las gráficas del \textit{Slow control} . Se realizó con la sala limpia, donde se sitúa el detector NEXT-DEMO, con la luz apagada.

Con esta prueba se llegó a la conclusión de que el vacío mínimo necesario para trabajar en un rango seguro es $5,5$ - $6 \times 10^{-3}$ mbar. Por tanto, los PMTs deben ser alimentados con alta tensión solo en el caso de haber 1 bar o un vacío mejor de $5,5$ - $6 \times 10^{-3}$ mbar.

\begin{figure}[!h]
 \centering
  \subfloat[Plano de energía de DEMO durante el test.]{
  \label{fig:chispas_EP}
     \includegraphics[width=0.4\textwidth]{IMG/Chapter3/pashen_EP}}
    \hspace{2mm}
  \subfloat[Detalle de la ventana donde se puede ver la base durante el test.]{
  \label{fig:pashen_detalle}
    \includegraphics[width=0.32\textwidth]{IMG/Chapter3/pashen_detalle}}
 \caption{Test de chispas y vacío.}
 \label{base:pashen}
\end{figure} 
 
 
 \begin{figure}[!h]
	\centering
	\includegraphics[width=0.7\textwidth]{IMG/Chapter3/pashen_chispa}
	\caption{Momento en que salta chispa en la base} 
	\label{fig:pashen_chispas}
\end{figure}




% -------------------------------------------------
% -------------------------------------------------
% -------------------------------------------------
