#!/bin/sh

# Usage:
# ./genera_EPUB.sh "nombre archivo (sin extesi�n)" "Autores separados por '&'" "T�tulo de la publicaci�n" "MathML"

if [ $4 = "MathML" ]; then
	echo
	echo "----------------------------------------------------------------------"
	echo "Se va a generar el archivo $1.epub con las ecuaciones en formato MathML"
	echo "----------------------------------------------------------------------"
	echo
	htlatex $1 "./EPUB_config.cfg,xhtml,mathml" " -cunihtf" "-cvalidate"
else
	echo
	echo "-------------------------------------------------------------------"
	echo "Se va a generar el archivo $1.epub con las ecuaciones en formato PNG"
	echo "-------------------------------------------------------------------"
	echo
	htlatex $1 "./EPUB_config.cfg,html"
fi

ebook-convert $1.html $1.epub --extra-css $1.css --cover EPUB_portada.png --level1-toc "//h:h2" --level2-toc "//h:h3" --page-breaks-before "//h:h2" --no-chapters-in-toc --book-producer "Universitat Polit�cnica de Val�ncia" --language "spanish" --authors "$2" --title "$3"
