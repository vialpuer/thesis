\begin{savequote}[60mm]
"The scientific man does not aim at an immediate result. He does not expect that his advanced ideas will be readily taken up. His work is like that of the planter - for the future. His duty is to lay the foundation for those who are to come, and point the way."
\qauthor{-- Nikola Tesla}
\end{savequote}

\chapter{The NEXT Experiment}\label{sec:next}

The Neutrino Experiment with a Xenon TPC (NEXT), will search for the neutrinoless double beta decay of $^{136}$Xe using a radiopure high-pressure xenon gas Time Projection Chamber (TPC) filled with $100~kg$ of Xe enriched in its $^{136}$Xe isotope. The experiment will be located at \emph{Laboratorio Subterr\'aneo de Canfranc}, which is carved into the rock at $850$~meters deep below the Tobazo Mountain, on the Spanish side of the Pyrenees.
NEXT will be the first large high-pressure gas TPC to use electroluminescence readout with SOFT (Separated, Optimized Functions TPC) technology. The following sections outline the most relevant aspects of the experiment, summarizing the neutrinoless double beta decay, 


% -------------------------------------------------
%	Neutrinoless Double Beta Decay
% -------------------------------------------------
\section{Neutrinoless Double Beta Decay ($\beta\beta 0 \nu$)}

The Standard Model (SM) of Particle Physics is a theory concerning the electromagnetic, weak, and strong nuclear interactions, which describes fundamental properties of subatomic particles. This model works well in general, but there are still some missing pieces, many of them related to neutrinos. These particles are unique in many ways; in particular, their lack of color or electromagnetic charge means that, of the three fundamental forces described by the SM, they only feel the weak force. The Standard Model requires massless neutrinos in its basic formulation, but recent experiments on neutrino oscillations have demonstrated that they are massive particles, opening a new field of physics beyond the Standard Model.

Neutrinos might be the only particles having a Majorana mass term, forbidden to the other fermions, explaining the different mass scale of neutrinos compared with other fundamental particles. Experimental evidence of this phenomenon would have deep implications in physics and cosmology, since Majorana particles would be their own antiparticles as described by Majorana \parencite{Majorana:1937vz}.

If neutrinos are Majorana particles, neutrinoless double beta decay ($\beta\beta0\nu$) could be observable and a mean to prove this hypothesis. The simple existence of $\beta\beta0\nu$ decay would prove that neutrinos are Majorana particles and that lepton number is not always conserved, while the decay rate would measure the neutrino mass.

Double beta decay ($\beta\beta$) is a very rare nuclear transition in which a nucleus with $Z$ protons decays into a nucleus with $Z + 2$ protons and same mass number A. It can only be observed in those isotopes where the $\beta$ decay mode is forbidden due to the energy of the daughter nuclei being higher than the energy of the parent nuclei, or highly suppressed. If this condition is fulfilled, two simultaneous $\beta$ decays are possible.

\begin{figure}[!ht]
	\bigskip
	\centering
	\includegraphics[width=0.9\textwidth]{bb0nu1.png}
	\caption{\textit{Atomic masses of isotopes with $A = 136$ given as differences with respect to the most bound isotope, $^{136}$Ba. The red levels indicate odd-odd nuclides, whereas the green indicate even-even ones. The arrows show the type of nuclear transition connecting the levels. Double beta (either plus or minus) transitions are possible because the intermediate state ($\Delta$Z = $\pm$1) is less bound, forbidding the beta decay \parencite{Martin-Albo:2015dza}.}}
			\label{fig:next:bb0nu1}
\end{figure}

Two $\beta\beta$ decay modes are normally considered. The standard two neutrino double beta decay mode ($\beta\beta2\nu$) was proposed by Goeppert-Mayer in 1935 and has been observed in several nuclei, where an anti-neutrino associated to each electron is emitted. This process has typical lifetimes on the order of $10^{18}-10^{21}$ years.

In the neutrinoless double beta decay mode ($\beta\beta0\nu$) the electrons carry essentially all the energy released in the decay. This process, which was postulated by Furry in 1939 and has not been observed yet, is forbidden in the Standard Model of Particle Physics. The Feynman diagrams for both possible decays are represented on figure \ref{fig:next:bb0nu2}.

\begin{figure}[!ht]
	\bigskip
	\centering
	\includegraphics[width=0.9\textwidth]{feynman2b.jpg}
	\caption{\textit{Feynman diagram for the $\beta\beta2\nu$ (left) and the $\beta\beta0\nu$ (right).}}
			\label{fig:next:bb0nu2}
\end{figure}

Only if neutrinos are massive, Majorana particles, and therefore their own antiparticles, $\beta\beta0\nu$ can take place. The anti-neutrino created in a vertex from one $\beta$ decay virtually propagates to the other vertex, where it acts as a neutrino producing an electron via inverse beta decay. As the neutrino acts in a vertex as a neutrino and in the other as an anti-neutrino, this process is only possible if both particles are the same. Additionally, the observation would demonstrate that total lepton number is violated in physical phenomena, an observation that could be linked to the cosmic asymmetry between matter and antimatter through the process known as leptogenesis.

\begin{figure}[!ht]
	\bigskip
	\centering
	\includegraphics[width=0.5\textwidth]{energia.png}
	\caption{\textit{Energy spectrum of the electrons emitted in the $\beta\beta$ decay of $^{136}$Xe, as seen with a $1\%$ FWHM energy resolution at Q$_{\beta\beta}$. The left peak corresponds to the $\beta\beta2\nu$ decay, while the right peak, centered at Q$_{\beta\beta} = 2458~keV$, corresponds to the $\beta\beta0\nu$. The normalization scale between the two peaks is arbitrary.}}
			\label{fig:next:bb0nu3}
\end{figure}

In the case of $\beta\beta0\nu$, the sum of the kinetic energies of the two released electrons is always the same, and corresponds to the mass difference between the parent and the daughter nuclei: $Q_{\beta\beta} \equiv M(Z, A) - M(Z + 2, A)$. However, due to the finite energy resolution of any detector, $\beta\beta0\nu$ events are reconstructed within a non-zero energy range centered around $Q_{\beta\beta}$, typically following a gaussian distribution, as shown in figure \ref{fig:next:bb0nu3}. Other processes occurring in the detector can fall in that region of energies, thus becoming a background and compromising drastically the experiment's expected sensitivity to $m_{\beta\beta}$.

\subsubsection{$\beta\beta$ Eperiments}

The main goal of basically all double beta decay experiments is to measure the total energy of the radiation emitted by a $\beta\beta$ source. In a neutrinoless double beta decay, the sum of the energies of the two emitted electrons is constant and equal to the mass difference between the parent and the daughter atoms ($Q_{\beta\beta}$). Any experiment hoping to measure the $\beta\beta0\nu$ half-life must be able to count the number of events at this energy due to $\beta\beta0\nu$. However, the measurement is limited by the experimental sensitivity of the detector employed.

Natural fluctuations and detector effects combine to smear the energy response and backgrounds from naturally occurring radioisotopes can pollute the energy region. For that reason, the materials with which the detector is built must be selected carefully to reduce the natural radioactivity present in all materials. In addition, double beta decay experiments must be placed at underground facilities, in order to reduce the background levels from atmospheric radiation. Finally, the intrinsic background from the standard two neutrino double beta decay mode ($\beta\beta2\nu$), which has a continuous energy spectrum, can be problematic if the energy resolution is not very good.

Three experiments of the present generation are taking data already:

On the one hand, the GERDA experiment \parencite{Lehnert:2014pma} looks for the neutrinoless double beta decay of $^{76}$Ge at \emph{Laboratori Nazionale del Gran Sasso}. In GERDA, high purity germanium detectors (HPGe) are arranged in strings and mounted in special low-mass holders made of ultra-pure copper and PTFE. The strings are suspended inside a vacuum-insulated stainless steel cryostat of $4.2~m$ diameter and $8.9~m$ height filled with $64~m^3$ of liquid argon. A copper lining $6~cm$ thick covers the inner cylindrical shell of the cryostat. The cryostat is placed in a $590~m^3$ water tank instrumented with PMTs which serves as a Cherenkov muon veto as well as a gamma and neutron shield.
The GERDA Collaboration has published a measurement of the $\beta\beta2\nu$ half-life of $^{76}$Ge, $T^{2\nu}_{1/2} (^{76}Ge) = (1.926 \pm 0.095) \times 10^{21}$ years \parencite{Agostini:2015nwa} and a limit on the $\beta\beta0\nu$ half-life, $T^{0\nu}_{1/2} (^{76}Ge) = > 2.1 \times 10^{25}$ years ($90\%$ C.L.) \parencite{Palioselitis:2015xca}.

On the other hand, there are xenon-based detectors like KamLAND-Zen \parencite{Gando:2012zm}, a transparent nylon-based balloon with $3.08~m$ diameter, containing $13$ tons of liquid scintillator loaded with $320~kg$ of xenon (enriched to $91\%$ in $^{136}$Xe). The balloon is suspended by film straps at the center of a stainless steel spherical vessel with $1879$ photomultiplier tubes mounted on the inner surface, which record the scintillation light generated by $\beta\beta$ events occurring in the detector. With this configuration it has achieved an extrapolated energy resolution of $9.9\%$ FWHM at the $Q_{\beta\beta}$ value of $^{136}$Xe, publishing recently a limit on the half-life of $\beta\beta0\nu$ of $T^{0\nu}_{1/2} (^{136}Xe) > 1.9 \times 10^{25}$ years.

In parallel, the EXO Collaboration has published a limit on the half-
life of $\beta\beta0\nu$ of $T^{0\nu}_{1/2} (^{136}Xe) > 1.6 \times 10^{25}$ years \parencite{Auger:2012ar} using the EXO-200 detector, a symmetric TPC filled with $110~kg$ of liquid xenon (enriched to $80.6\%$ in $^{136}$Xe). In EXO-200, ionization charges in the xenon created by charged particles drift towards the two anodes of the TPC due to the presence of an electric field. Events in the chamber are reconstructed by a pair of crossed wire planes which measure their amplitude and transverse coordinates, and an array of avalanche photodiodes (APDs), which detect the $178~nm$ xenon scintillation light. The sides of the chamber are covered with teflon sheets that act as VUV reflectors, improving the light collection. The EXO-200 detector has achieved an energy resolution of $4\%$ FWHM at the $Q_{\beta\beta}$ value of $^{136}$Xe.

The combination of the KamLAND-Zen and EXO results give a limit of $T^{0\nu}_{1/2} (^{136}Xe) > 3.4 \times 10^{25}$ years ($120-250~meV$, depending on the NME) \parencite{Gando:2012zm}.% This result excludes the claim of Klapdor-Kleingrothaus and collaborators [56].

In the following section, a new neutrinoless double beta decay experiment is
introduced: the NEXT experiment, which will search for neutrinoless double beta decay of $^{136}$Xe at \emph{Laboratorio Subterr\'aneo de Canfranc} with the NEXT-100 detector. Such a detector, containing $100~kg$ of $^{136}$Xe, thanks to its excellent and demonstrated energy resolution, together with a high efficiency background rejection, will be one of the leading experiment in the field, exploring the region of neutrino mass lower than $100~meV$.


% -------------------------------------------------
% -------------------------------------------------
% -------------------------------------------------
