\begin{savequote}[60mm]
"Todos tenemos el poder de elegir, aunque el destino esté marcado no lo está el porvenir"
\qauthor{-- David Martínez Álvarez}
\end{savequote}

\chapter{Electrónica de acondicionamiento}\label{sec:FEE}

Una de las características más distintivas de una \HPXeEL\ es su excelente resolución de energía, con un límite intrínseco (dado por el Fano factor) de aproximadamente 0.3 \% FWHM en el punto final de xenon \Qbb\ (2.458 MeV). Los prototipos de NEXT \parencite{Alvarez:2012hh} , así como los resultados iniciales de \NEW\ \parencite{Renner:2018ttw} también han demostrado una resolución de energía para partículas puntuales (rayos X de Krypton) que se extrapola a 0.5 \% at \Qbb\ (asumiendo una escala ingenua $\sigma_{\Qbb} \sim \sigma_{Kr}/\sqrt{E}$) y mejor que $\sim 0.7$ \% FWHM para trazas extendidas.

La electrónica de acondicionamiento para el plano de energía de \NEW\ ha sido completamente desarrollada por nosotros y fue diseñada para preservar la resolución intrínseca que caracteriza a una \HPXeEL . Se ha elegido un esquema de cátodo conectado a tierra para simplificar el diseño estructural y eliminar la necesidad de un aislamiento de la carcasa, tal y como se explicó en secciones anteriores. Además, se ha logrado una notable reducción de los efectos de ruido de baja frecuencia utilizando esta estrategia de conexión.

\section{Requerimientos de diseño}

El diseño final personalizado se muestra en \figu\ \ref{fig:FEE_scheme}. Se ha diseñado una transmisión pseudo-diferencial para reducir el ruido acoplado, que se espera que sea alto, ya que la distancia entre el armario de la electrónica donde se situa el FEE y la vasija de presión \NEW\ es de aproximadamente 12 metros. El cable elegido fue Twinax TWC-124-2, dicho cable consta de un par de lineas trenzadas con $124\Omega$ de impedancia de par, ambas lineas apantalladas en su conjunto y que puede trabajar a tensión de operación 2Kv de tensión continua. La colocación del condensador de acoplo C2 permite el uso del mismo cable tanto para la tensión de alimentación como para la extracción de la señal. Para lograr una línea de TX adecuada que coincida con una terminación de línea de tipo $\pi$ se ha utilizado implícitamente la impedancia de entrada del amplificador definida por RA y RB. Tanto la impedancia diferencial de ($120\Omega$) como la impedancia de modo común de ($50\Omega$) se han definido en función de las características de la línea TX. La terminación del modo común es de gran importancia ya que se está utilizando una conexión pseudo-diferencial en la base PMT y se espera que el ruido de las señales del modo común que viajan a lo largo de la línea de transmisión sean bastante altas.

\begin{figure*}[!htbp]
	\centering
		\includegraphics[width=1.1\textwidth]{IMG/Chapter4/FEE_simple_scheme.pdf}
		\caption {Esquema simplificado del FEE.} 
		\label{fig:FEE_scheme}
\end{figure*}

\section{Solución de alto valor $\tau$ constante.}\label{cap:tau}

Dado que el $\tau$ del circuito equivalente que se muestra en la fig. \ref{fig:grounded_cathode} es igual a la inversa de $f_{cutoff}$, una mayor $\tau$ atenuará un rango más corto de frecuencias produciendo una señal que se asemeja mucho a la original. Sin embargo, hay un problema con la implementación de esta solución. Los condensadores de acoplo AC de buena calidad que pueden soportar altos voltajes suelen tener valores nominales bajos. Es difícil encontrar condensadores más altos de 10 nF que cumplan con dichas especificaciones. Entonces deben usarse resistencias de valor alto. El siguiente ejemplo muestra cómo calcular el valor de resistencia necesario en función de la pérdida de señal en la amplitud que tiene un efecto directo en la resolución de energía.


\begin{description}
	\item[Ejemplo: Valor de C en función de la variación de la linea base]
	El cambio de línea base debido al cambio de tensión en el condensador de acoplo AC se puede calcular como la carga y descarga de la tensión a través de sus terminales debido a un cambio de señal en la entrada. La curva de \mbox{carga / descarga} de un condensador está relacionada con su $\tau$ en el circuito y sigue la ecuación. \ref{eq:2}.
	
	\begin{equation}
	\tau=\frac{1}{(R_1+Z_{in})C}
	\end{equation}
	
	\begin{equation}
	\Delta V_C=V_C.(1-e^{-t\tau})
	\label{eq:2}
	\end{equation}
	
	Para un $\Delta V_C = 0.25\%$ de $V_C$ que se traduciría aproximadamente en un error de resolución de energía de $0.5\%$ y pulsos de una longitud de aproximadamente $200\mu s$ el valor de resistencia mínimo permitido para un condensador de 10 nF es igual a 8 M$\Omega$ .

	Esto significa que para mejores mediciones de resolución de energía serían deseables valores más altos de resistencia (se recomendarían al menos M$\Omega $)
\end{description}

Tales resistencias de alto valor pueden amplificar componentes de baja frecuencia como se muestra en la ec. \ref{eq:full_fee}. Este efecto se traducirá en desplazamientos de línea de base adicionales (\figu\ \ref{fig:Full_Freq_high_tau}) o desviación de la línea de base.

\begin{figure}
	\begin{center}
		\includegraphics[width=0.8\textwidth]{IMG/Chapter4/tau_badidea.png}
		\caption{Respuesta de frecuencia completa del FEE para un alto valor $\tau$}
		\label{fig:Full_Freq_high_tau}
	\end{center}
\end{figure}

\section{Etapa de alimentación}

Una parte importante de la tarjeta electrónica para el acondicionamiento de la señal es la parte de alimentación de dicha tarjeta, para evitar daños a la electrónica, la alimentación debía poseer protección contra sobrecorrientes, y además un sistema de filtrado que garantizase una tensión de alimentación estable y sin ruido.

Para los componentes que forman el circuito, es suficiente con proveer tensiones de $\pm5$V, y para cumplir con ello se eligieron los modelos LT1965IQ, para el caso de $+5$V, y de LM2991S, para -5V para los reguladores de tensión. Para la elección de estos hizo una estimación del consumo de corriente previsto para la tarjeta, sabiendo el consumo de un canal se estimó el consumo total de la tarjeta con 8 canales. Se estimó por exceso, asumiendo que el consumo máximo de esta tarjeta sería inferior a 500mA, en el caso de la linea de -5V, y de 1A en el caso de la linea de $+5$V. 

Además se buscó, en la medida de lo posible, que la caída de tensión a la entrada del regulador fuese la menor posible, ya que esto reduce el consumo de dicho componente y evita el sobrecalentamiento. Ambos reguladores se encuentran dentro del grupo de los llamados LDO (Low Voltage Dropout), en el caso del regulador LM2991S, la caída de tensión entre entrada y salida es de 0,6V y en el LT1965IQ de 0,31V. Además, estos reguladores permiten ajustar la tensión de salida mediante la correcta asignación de tensión en el terminal de ajuste. Esto se hace mediante un divisor resistivo, tal y como se muestra en las figuras  \ref{fig:regu1} y \ref{fig:regu2}.

%\begin{figure}
%	\begin{center}
%		\includegraphics[width=0.4\textwidth]{IMG/Chapter4/regu2}
%		\caption{Ajuste del regulador de -5V (datasheet de Texas instruments)}
%		\label{fig:regu1}
%	\end{center}
%\end{figure}
%
%\begin{figure}
%	\begin{center}
%		\includegraphics[width=0.4\textwidth]{IMG/Chapter4/regu1}
%		\caption{Ajuste del regulador de +5V (datasheet de Linear technologies)}
%		\label{fig:regu2}
%	\end{center}
%\end{figure}

\begin{figure}[H]
 \centering
  \subfloat[Ajuste del regulador de -5V (datasheet de Texas instruments)]{
  \label{fig:regu1}
     \includegraphics[width=0.6\textwidth]{IMG/Chapter4/regu2}}
    \vspace{8mm}
  \subfloat[Ajuste del regulador de +5V (datasheet de Linear technology)]{
    \label{fig:regu2}
    \includegraphics[width=0.5\textwidth]{IMG/Chapter4/regu1}}
    \caption{Detalle de los ajustes para la regulación de las tensiones de entrada.}
 \label{FEE:reguladores}
\end{figure}

Según indica el fabricante del regulador LM2991S, la resistencia R2 debe ser de $100k\Omega$ debido a que utilizando la resistencia inferior a 100 $k\Omega$ hace que el error debido a la corriente de pin de ajuste sea despreciable. La resistencia R1 se puede obtener de la expresión:


\begin{equation}
V_{OUT} = V_{ADJ} \cdot (1 + \frac{R2}{R1})
\end{equation}

Donde $V_{ADJ}$ se fija internamente y es $V_{ADJ}= -1,21V$.

De donde obtenemos, por aproximación al valor estándar más próximo, una resistencia R1 de 30k$\Omega$.

En el caso del regulador LT1965IQ, las resistencias R2 y R1 se pueden obtener de la expresión:

Donde $V_{ADJ} = 1,20V$ y $I_{ADJ} = 1,3\mu A$ a $25\degree$.

\begin{equation}
V_{OUT} = V_{ADJ} \cdot (1 + \frac{R2}{R1}) + I_{ADJ} \cdot R2
\end{equation}

de donde obtenemos, una resistencia R1 de 24k$\Omega$ y R2 de 75k$\Omega$.

Respecto a la protección del circuito, se añadieron fusibles autorrearmables a la entrada de cada linea de alimentación. Se consideró adecuado un margen de seguridad mínimo del 25\% por ello, de entre los disponibles, se eligieron fusibles de 1A y 1,5A respectivamente. Estos fusibles desconectan la entrada en caso de excederse la corriente máxima, pero vuelven a establecer la conexión cuando se desconecta la alimentación durante un pequeño espacio de tiempo.

A continuación, en las figuras \ref{fig:alimentacion_mas} y \ref{fig:alimentacion_menos} puede verse la etapa de alimentación.

\begin{figure}
	\begin{center}
		\includegraphics[width=1.1\textwidth]{IMG/Chapter4/alimentacion_mas}
		\caption{Esquema de la alimentación de +5V.}
		\label{fig:alimentacion_mas}
	\end{center}
\end{figure}

\begin{figure}
	\begin{center}
		\includegraphics[width=1.1\textwidth]{IMG/Chapter4/alimentacion_menos}
		\caption{Esquema de la alimentación de -5V.}
		\label{fig:alimentacion_menos}
	\end{center}
\end{figure}

En lo referente al filtrado de la alimentación, es exactamente el mismo para las dos tensiones de entrada. En la primera etapa se encuentra un condensador de 100nF, como primer filtrado de componentes frecuenciales altas, y el fusible comentado anteriormente como protección. Tras el filtrado se incluyó un choque de modo común, el cual bloquea las corrientes en modo común entre la alimentación y masa, evitando los perjudiciales bucles de masa. A continuación están los reguladores de tensión, con etapas de filtrado tanto para bajas como altas frecuencias, así como una ferrita que atenúa los cambios bruscos de corriente que pueden producir niveles de tensión incorrectos en las líneas de alimentación. Con estos filtros se evita en gran medida la aparición de ruido, así como gran cantidad de armónicos.

\section{Rama de energía}

\subsection{Antecedentes}

Para hablar de la parte realmente importante del \textit{FEE}, la rama de energía, se van a comentar varias versiones del mismo antes de la versión final, la versión 4. 

En esta sección se va empezar desde la versión 3, ya que es el punto de partida desde el que se empezó a trabajar para esta tesis. En las versiones anteriores a la versión 3, el concepto de \textit{FEE} se separaba en 2 tarjetas electrónicas, la tarjeta de acondicionamiento con la cual se alimentaba el PMT y se realizaba el acoplo para quitar el nivel de continua de la señal a tratar, y el propio \textit{FEE} que contenía la rama de energía. A partir de la versión 3 se decidió unir todo en una única tarjeta electrónica. 

A continuación de detallan y desarrollan las diferentes versiones así como la versión que está actualmente instalada y funcionando en el detector \NEW .

\subsection{FEE v3}

Esta versión nace de unir las dos tarjetas electrónicas como se ha comentado anteriormente en una sola y añadir un diodo zener a modo de protección contra subidas de tensión, tratando de evitar dañar el circuito posterior. En la \figu\ \ref{fig:FEEv3} se puede ver el esquema descrito anteriormente. 

Las resistencias R19 \& R20, además de los condensadores C7 \& C8 se utilizan para filtrar la alta tensión que alimenta a los PMTs. En esta versión se pusieron diferentes huellas para condensadores de acoplo para de ese modo poder probar varias combinaciones. Z1 \& Z2 se decidieron poner como protección ante picos de tensión para evitar dañar la electrónica del canal. El amplificador diferencial utilizado en esta versión es el THS4511, al ser el mismo modelo que finalmente se puso en la versión final del \textit{FEE}, esta parte se explicará detalladamente en la sección \ref{FEEv4}. Por su parte, el driver diferencial con salida adaptada a $50\Omega$ para proporcionar la corriente necesaria para la correcta transmisión de la señal hasta el DAQ en el \textit{FEEv3} era el AD8131 de la compañia \textit{Texas Instruments}. 

\begin{figure}[H]
	\begin{center}
		\includegraphics[angle=90, width=0.5\textwidth]{IMG/Chapter4/FEE_PMT_v3_rama.pdf}
		\caption{Esquema de la rama de alimentación del FEEv3}
		\label{fig:FEEv3}
	\end{center}
\end{figure}

\subsection{FEE v3.2}

En esta versión se decidió añadir además de lo anterior una segunda etapa de amplificación, para poder amplificar más en caso necesario. También se añadió filtrado extra, así como ajuste del offset de mediante un potenciometro, este ajuste se realiza mediante las resistencias R29, R30 \& R31, junto con C20 y el un potenciometro de $5K\Omega$ . En esta versión se mantuvo el driver diferencial a la salida del canal (\figu\ \ref{fig:FEEv3.2}).

\begin{figure}[H]
	\begin{center}
		\includegraphics[angle=90, width=0.5\textwidth]{IMG/Chapter4/FEE_PMT_v3_2.pdf}
		\caption{Esquema de la rama de alimentación del FEEv3.2}
		\label{fig:FEEv3.2}
	\end{center}
\end{figure}

Con tal de mejorar el rendimiento del \textit{FEE} se le realizaron modificación manuales a esta tarjeta electrónica, y tras confirmar mediante su uso la mejora del mismo se procedió a mejorar el diseño anterior llegando a la versión 4 que se explica detalladamente a continuación (\figu\ \ref{fig:FEEv3.2_a} y \figu\ \ref{fig:FEEv3.2_b}).

\begin{figure}[H]
	\begin{center}
		\includegraphics[angle=90, width=.7\textwidth]{IMG/Chapter4/FEE_PMT_v3_2_fotoB.jpg}
		\caption{Fotografía del FEEv3.2 con modificaciones posteriores (Top)}
		\label{fig:FEEv3.2_a}
	\end{center}
\end{figure}

\begin{figure}[H]
	\begin{center}
		\includegraphics[width=.7\textwidth]{IMG/Chapter4/FEE_PMT_v3_2_fotoA.jpg}
		\caption{Fotografía del FEEv3.2 con modificaciones posteriores (Bottom)}
		\label{fig:FEEv3.2_b}
	\end{center}
\end{figure}




\subsection{FEE v4}\label{FEEv4}

Los componentes activos en el FEE se eligieron para minimizar el ruido. Se optó por utilizar la transmisión diferencial, ya que de esta manera es posible cancelar el ruido acoplado a la señal. El amplificador diferencial elegido fue el THS4511 de Texas Instruments (\figu\ \ref{fig:sch_ch}), que se caracteriza por alto ancho de banda, bajo ruido y baja distorsión. Después de la amplificación diferencial, la señal pasa a través de un filtro pasivo de paso-bajo. Finalmente, se utiliza un controlador (\textit{driver}) diferencial (LT6600 de \textit{Analog devices}) que proporciona suficiente corriente para la transmisión al sistema de adquisición externo. El driver se decidió cambiar respecto a la versión anterior, ya que, este nuevo modelo utiliza la misma numeración de pin pero añade la funcionalidad de poder fijar un tensión DC de referencia directamente al driver. Además este último tiene un filtro paso-bajo de 5MHz integrado en el componente. 


\begin{figure}[h!]
	\begin{center}
		\includegraphics[angle=90, width=.5\textwidth]{IMG/Chapter4/sch_ch.pdf}
		\caption{Esquema de la rama de energía}
		\label{fig:sch_ch}
	\end{center}
\end{figure}

Según la configuración vista en el esquema, las resistencias R4 y R5 son las que definen la ganancia de la etapa amplificadora, pues se trata de un amplificador de transresistencia. Además, dichas resistencias deben ser del mismo valor, para que la amplitud de la señal en las dos lineas diferenciales sea la misma. En nuestro caso, se utilizaron resistencias de valor 1k5, teniendo de ese modo una amplificación en corriente de 1500. Por otro lado, las resistencias R2 y R3 también deben ser iguales entre sí. Si esto último no se cumple, las ramas diferenciales estarían desequilibradas y se vería reflejado en un offset adicional a la salida. Mediante simulaciones, se determinó que el valor de las resistencias R2 y R3 debía ser de $82\Omega$ .

Tras el amplificador se encuentra el filtro paso-bajo, que es de gran importancia, pues además de atenuar ruido de alta frecuencia hace que la señal sea más ancha. Por requerimientos del experimento se necesita que una señal producida por un SPE (Single Photo Electron) se prolongue por encima de los 100ns. Como la descarga de un condensador responde a una curva exponencial, se decidió tomar como punto final de la señal cuando el valor descendiese por debajo del 25\% de la carga máxima. A partir de la ecuación de descarga del condensador puede calcularse que, para que a los 100ns se haya descargado el 75\% de la tensión, la constante de tiempo $\tau$ debe ser:

\begin{equation}
V = V_o e^{- \frac{t}{\tau}}
\end{equation}

\begin{equation}
\tau = - \frac{t}{ln( \frac{V}{V_o})} = - \frac{100ns}{ln 0,25} = 72ns
\end{equation}


Para este filtro diferencial tenemos que tener en cuenta que la resistencia vista por el condensador $C1$ sería $(R6 \parallel (R18 + R10))$. Al ser un sistema diferencial para hacer el calculo de $\tau$ el valor del condensador que ven las resistencias será el doble $C1_1 = 440pF$ como se muestra en la \figu\ \ref{fig:LPF}.

\begin{figure}[H]
	\begin{center}
		\includegraphics[width=0.6\textwidth]{IMG/Chapter4/LPF}
		\caption{Detalle del filtro paso-bajo (Realmente solo existe un condensador $C_1$ de 220pF, en el dibujo se ven 2 debido a que cada rama ve como si tuviera su propio condensador ($C1_1$ y $C1_2$) del doble de capacidad)}
		\label{fig:LPF}
	\end{center}
\end{figure}

Aplicando los cálculos anteriores, la constante de tiempo viene dada por la expresión $\tau = (R6 \parallel (R18 + R10))\cdot C1_1$.

\begin{equation}
\tau = (R6 \parallel (R18 + R10))\cdot C1_1
\end{equation}

Utilizando $R6$ y $R16$ = $220\Omega$ y $R10 = 820\Omega$ , la ecuación es:

\begin{equation}
\tau = (220\Omega \parallel (220\Omega + 820\Omega))\cdot 440pF
\end{equation}
\begin{equation}
\tau = (\frac{220\Omega \cdot 1k04\Omega}{220\Omega + 1k04\Omega})\cdot 440pF
\end{equation}
\begin{equation}
\tau = 181,6\Omega \cdot 440pF = 79,9ns
\end{equation}

Como $\tau$ debía ser mínimo 72ns, con el valor de resistencias aplicadas en la ecuación anterior se cumple con dicha condición. Los valores finalmente asignados que se observan en la \figu\ \ref{fig:sch_ch}, se validaron mediante simulación y consiguen prolongar la señal por encima de los 250ns para una descarga del 75\% (\figu\ \ref{fig:LTspice}). 

\begin{figure}[H]
	\begin{center}
		\includegraphics[angle=90, width=.8\textwidth]{IMG/Chapter4/Simu_V}
		\caption{Gráfica de la simulación de la rama negativa con LTspice}
		\label{fig:LTspice}
	\end{center}
\end{figure}

\begin{figure}[H]
	\begin{center}
		\includegraphics[angle=90, width=.8\textwidth]{IMG/Chapter4/Simu_I_4}
		\caption{Gráfica de la simulación de la rama negativa con LTspice}
		\label{fig:LTspice1}
	\end{center}
\end{figure}

%
%\begin{figure}[H]
% \centering
%  \subfloat[Gráfica de la simulación de la rama negativa con LTspice]{
%  \label{fig:dif_signals}
%     \includegraphics[width=0.9\textwidth]{IMG/Chapter4/Simu_V}}
%  \subfloat[Gráfica de la simulación de la rama negativa con LTspice]{
%    \label{fig:deco}
%    \includegraphics[width=0.9\textwidth]{IMG/Chapter4/Simu_I_4}}
% \caption{Gráfica de la simulación de la rama negativa con LTspice}
% \label{fig:LTspice}
%\end{figure}


\begin{figure}[H]
	\begin{center}
		\includegraphics[angle=90, width=.8\textwidth]{IMG/Chapter4/Simu_V_2}
		\caption{Gráfica de la simulación con LTspice}
		\label{fig:LTspice2}
	\end{center}
\end{figure}

\begin{figure}[H]
	\begin{center}
		\includegraphics[angle=90, width=.8\textwidth]{IMG/Chapter4/Simu_I_3}
		\caption{Gráfica de la simulación con LTspice}
		\label{fig:LTspice3}
	\end{center}
\end{figure}

%\begin{figure}[H]
% \centering
%  \subfloat[Gráfica de la simulación con LTspice]{
%  \label{fig:dif_signals}
%     \includegraphics[width=0.6\textwidth]{IMG/Chapter4/Simu_V_2}}
%  \subfloat[Gráfica de la simulación con LTspice]{
%    \label{fig:deco}
%    \includegraphics[width=0.6\textwidth]{IMG/Chapter4/Simu_I_3}}
% \caption{Gráfica de la simulación con LTspice}
% \label{fig:LTspice}
%\end{figure}

\begin{equation}
f_{cutoff} = \frac{1}{2\pi (R6 \parallel (R18 + R10))\cdot C1_1}
\end{equation}

\begin{equation}
f_{cutoff} = \frac{1}{2\pi (\frac{220\Omega \cdot 1k04\Omega}{220\Omega + 1k04\Omega})\cdot 440pF} = 2MHz
\end{equation}

La frecuencia de corte del filtro paso bajo es de 2MHz. El ancho de banda del FEE (2 MHz) lo hace funcionar como un filtro de conformación, extendiendo la duración del tiempo de respuesta de fotoelectrón único que fue proporcionado como una especificación por el equipo de calibración del detector.

Por otra parte, se ha introducido un ajuste de offset mediante un divisor resistivo y un potenciometro de 5 K$\Omega$ , este ajuste de offset se utiliza para poder tener control sobre la línea de base y evitar que las señales saturen, permitiendo de ese modo, aprovechar al máximo el rango dinámico de la FPGA del DAQ. 

Al ser diferencial, tenemos dos señales opuestas, debido a esto es posible que si la linea de base no está en lugar adecuado pueda saturar una de las dos señales, lo que equivale a una deformación en la señal final tras restarlas y después de ser decodificada en el DAQ. Lo que permite el potenciometro es poder separar ambas señales con una diferencia constante entre ellas (\figu\ \ref{fig:dif_signals1}), y así situar la linea de base en un lugar diferente a cero, ampliando de ese modo el rango dinámico para nuestra señal (\figu\ \ref{fig:deco1}). 


\begin{figure}[H]
 \centering
  \subfloat[Explicación modo común. Línea base de + y - ]{
  \label{fig:dif_signals}
     \includegraphics[width=0.4\textwidth]{IMG/Chapter4/modo_comun}}
    \hspace{2mm}
  \subfloat[Baseline en 0V, ya que línea base de + y - se sitúan en la misma tensión, y por tanto la diferencia es 0]{
    \label{fig:deco}
    \includegraphics[width=0.4\textwidth]{IMG/Chapter4/Baseline_0}}
 \caption{Explicación gráfica del ajuste del modo común}
 \label{FEE:baseline}
\end{figure}

\begin{figure}[H]
 \centering
  \subfloat[Diferencia constante del modo común de las líneas base de + y - ]{
  \label{fig:dif_signals1}
     \includegraphics[width=0.4\textwidth]{IMG/Chapter4/modo_comun_diff}}
    \hspace{2mm}
  \subfloat[Baseline separada de 0V, ya que línea base de + y - se sitúan a una diferencia constante ajustada en el FEE]{
    \label{fig:deco1}
    \includegraphics[width=0.4\textwidth]{IMG/Chapter4/Baseline_diff}}
 \caption{Explicación gráfica del ajuste del modo común}
 \label{FEE:baseline1}
\end{figure}

\begin{figure}[H]
 \centering
  \subfloat[Muestra de la saturación de una de las dos señales]{
  \label{fig:deco2}
     \includegraphics[width=0.4\textwidth]{IMG/Chapter4/diff_sat}}
    \hspace{2mm}
  \subfloat[Muestra de la señal obtenida de la resta de ambas señales, una de ellas saturada. Se puede ver el efecto de la saturación.]{
    \label{fig:deco3}
    \includegraphics[width=0.4\textwidth]{IMG/Chapter4/diff_sat_baseline}}
 \caption{Explicación del efecto saturación debido a la posición de la linea base.}
 \label{FEE:baseline2}
\end{figure}

En la \figu\ \ref{fig:baseline_0} se puede observar una señal en 2050 cuentas aproximadamente, eso es debido a que tanto la señal positiva como la señal negativa se sitúan en el modo común.

\begin{figure}[H]
	\begin{center}
		\includegraphics[width=.5\textwidth]{IMG/Chapter4/modo_comun_0V}
		\caption{Baseline en 2050 cuentas, lo que equivalente aproximadamente a 0, ya que el rango completo son 400 cuentas de ADC}
		\label{fig:baseline_0}
	\end{center}
\end{figure}

A continuación se muestra las dos señales reales de \textit{FEE} con su correspondiente imagen de la señal procesada después del sistema de adquisición, en las figuras \ref{FEE:baseline3} y \ref{FEE:baseline4} se muestra como cambia la linea de base dependiendo de donde se situen ambas señales. Esto puede ser crucial para que no sature la señal de salida deformando la señal procesada.

\begin{figure}[H]
 \centering
  \subfloat[Diferencia entre señales a la salida del FEE]{
  \label{fig:deco2}
     \includegraphics[width=0.4\textwidth]{IMG/Chapter4/modo_comun_baseline_2130}}
    \hspace{2mm}
  \subfloat[Baseline en 2130 debido a la distancia entre señales del modo común.]{
    \label{fig:deco3}
    \includegraphics[width=0.5\textwidth]{IMG/Chapter4/baseline_2130}}
 \caption{Explicación del ...}
 \label{FEE:baseline3}
\end{figure}

\begin{figure}[H]
 \centering
  \subfloat[Diferencia entre señales a la salida del FEE]{
  \label{fig:deco2}
     \includegraphics[width=0.4\textwidth]{IMG/Chapter4/modo_comun_baseline_2660}}
    \hspace{2mm}
  \subfloat[Baseline en 2660 debido a la distancia entre señales del modo común.]{
    \label{fig:deco3}
    \includegraphics[width=0.5\textwidth]{IMG/Chapter4/baseline_2660}}
 \caption{Explicación del ....}
 \label{FEE:baseline4}
\end{figure}


Otra parte importante de la rama de energía, son los condensadores de acoplo AC (C11 \& C12), estos han sido elegidos de 18 nF con un dieléctrico C0G, porque ofrecen excelentes propiedades como alta estabilidad térmica, alta capacidad de corriente de ondulación, bajo cambio de capacitancia con respecto al voltaje de DC aplicado y sin disminución de capacidad con el tiempo. %Modern C0G (NP0) formulations contain neodymium, samarium and other rare earth oxides. C0G (NP0) ceramics offer one of the most stable capacitor dielectrics available. Capacitance change with temperature is 0 $\pm30ppm/$ $^{\circ}$C which is less than $\pm0.3\%$ $^{\circ}$ from $-55$ $^{\circ}$C to $+125$ $^{\circ}$. Capacitance drift or hysteresis for C0G (NP0) ceramics is negligible at less than $\pm0.05\%$ versus up to $\pm2\%$ for films. Typical capacitance change with life is less than $\pm0.1\%$ for C0G (NP0), one-fifth that shown by most other dielectrics.

En cuanto a la protección, tenemos un diodo de protección ESD, modelo NUP4114UPXV6T1G. Con el intentamos evitar dañar el sistema de adquisición donde el daño podría ser irreparable. Diseñado para proteger contra daños debidos a eventos ESD o transitorios de sobretensión.

El filtrado en la alta tensión de entrada se realiza con dos filtros paso-bajo. Los valores de resistencia de R20 \& R21 es de 4K7$\Omega$ y de condensador los condensadores C7 \& C8 es de 2,2nF. A continuación se muestra el cálculo de la frecuencia de corte:


\begin{equation}
f_{cutoff} = \frac{1}{2\pi \cdot R \cdot C}
\end{equation}

\begin{equation}
f_{cutoff} = \frac{1}{2\pi \cdot 4k7 \cdot 2,2nF} = 15,4 kHz
\end{equation}

La frecuencia de corte del filtro paso-bajo en la entrada de la alimentación es de 15,4 kHz.

Para finalizar con el diseño de la rama de energía, comentar que se añadieron las resistencias R27 \& R28 para poder descargar la alta tensión, en caso de que se desconectara el cable del FEE con la alta tensión aplicada. Además, cabe destacar que las resistencias R24 \& R25 \& R26 junto al condensador C9 están en la entrada del canal para completar el divisor resistivo de la base de polarización, esto corresponde a la parte del divisor entre el último dínodo y el ánodo, ajustando de esta manera la tensión en el fotomultiplicador de manera correcta.

\section{Simulación en Pspice}

Para comprobar el correcto funcionamiento del diseño se procedió a realizar la simulación del circuito diseñado con el programa LTspice. En la \figu\ \ref{fig:simu_pspice} se puede observar el circuito a simular.

Se simuló desde la última etapa del PMT y para ello se utilizaron dos fuentes de corriente imitando el pulso de salida del ánodo y del último dínodo. El pulso  de corriente utilizado en un principio simula un SPE, aunque se hicieron pruebas con distintas configuraciones. 

\begin{figure}[H]
	\begin{center}
		\includegraphics[width=1\textwidth]{IMG/Chapter4/sch_simu}
		\caption{Esquema de la simulación en LTspice}
		\label{fig:simu_pspice}
	\end{center}
\end{figure}

En la \figu\ \ref{fig:ejem} se muestra la simulación a la salida del FEE con un pulso equivalente a un SPE en la entrada.

\begin{figure}[H] 
	\begin{center}
		\includegraphics[width=0.9\textwidth]{IMG/Chapter4/Simu_I_3}
		\caption{Ejemplo de resultados}
		\label{fig:ejem}
	\end{center}
\end{figure}


\section{Diseño mecánico}

Para poder colocar los conectores tanto de alimentación como de señal, y además poder ensamblar las tarjetas de FEE en un crate de tamaño U6, se procedío a mecanizar un kit de unidad enchufable tipo bastidor Schroff que acepta placas en formato Euroboard. Los paneles superior e inferior tienen perforaciones para carriles de guía (corriente de aire de aproximadamente el 60\%) para inserción de una placa de circuito impreso. Las medidas son 167 mm de profundidad, 70 mm de ancho y 262 mm la parte más alta. La caja metálica esta en conformidad con RoHS. En la \figu\ \ref{FEE:caja} se muestra un dibujo 3D de la caja y una foto de la misma montada. Dentro de la caja el \textit{FEE} se sujeta a la caja mediante separadores de métrica M3 de 20 mm de longitud y tornillos de cabeza cónica. En el caso de los conectores del frontal de la caja se eligieron por una parte, para alimentar de alta tensión los PMTs, conectores SHV que pueden trabajar hasta 5000V. Por otra parte, para poder utilizar un único cable para alimentar y obtener la señal del PMT se necesitaba un conector que tuviera 2 vivos y que soportara la tensión necesaria, el elegido fue el LEMO ERA.3S.432.CLL.


\begin{figure}[H]
 \raggedleft
  \subfloat[Dibujo 3D]{
  \label{fig:caja3D}
     \includegraphics[width=0.5\textwidth]{IMG/Chapter4/FEE_BOX_3D}}
  \subfloat[Caja montada]{
    \label{fig:caja1}
    \includegraphics[width=0.5\textwidth]{IMG/Chapter4/BOX}}
 \caption{Caja metálica mecanizada}
 \label{FEE:caja}
\end{figure}

Esta caja se encuentra en el armario electrónico (\figu\ \ref{fig:rack}) cerca del sistema de adquisición. Un cable HDMI es el medio físico elegido para la conexión entre la placa FEE y el DAQ, como este último, desarrollado por la colaboración RD-51 mucho antes de que se diseñara la tarjeta FEE, se decidió utilizar este cable debido a su estructura diferencial y su robustez. Una frecuencia de muestreo DAC de una década sobre el ancho de banda de la señal FEE (2 MHz) debería ser suficiente para adquirir señales de salida FEE con una distorsión de frecuencia despreciable incluso si se utilizan filtros Nyquist de orden bajo.

El equipo para suministrar la alta tensión a los PMT es un sistema de suministro de energía multicanal universal, modelo SY 1527 de CAEN con la tarjeta A1733P: voltaje de salida de 0-4 kV y <30 mVpp de rizado.  % and fully control: Programmable parameters for each power channel that include two voltage values (V0set, V1set) and two current limit values (I0set, I1set). The switching from one value to the other is performed via two external (NIM or TTL) input levels (VSEL, ISEL). The maximum rate of change of the voltage (Volt/second) may be programmed for each channel. Two distinct values are available, Ramp-Up and Ramp-Down. 

\begin{figure}[H]
 \raggedleft
  \subfloat[Armarios electrónicos del detector NEW]{
  \label{fig:rack}
     \includegraphics[width=0.45\textwidth]{IMG/Chapter4/racks}}
  \subfloat[Dibujo de la distribución dentro del armario electrónico]{
    \label{fig:rack_dibu}
    \includegraphics[width=0.5\textwidth]{IMG/Chapter4/racks_dibu}}
 \caption{Armarios electrónicos del experimento.}
 \label{FEE:racks}
\end{figure}

\section{Diseño de la PCB}

Para el diseño del Layout se tuvieron en cuenta varios factores. El primero de ellos fue el tamaño de la tarjeta electrónica y su forma, esta debía ser de una medida en concreto para poder integrarla en una caja metálica que se pudiera integrar en un crate de tamaño 6U. 

Se decidió que la tarjeta fuera de 2 capas con plano de masa en ambas caras. Además la masa de entrada y la del resto de la tarjeta se separaría a través de los chokes. Por otra parte, las pistas de alimentación se hicieron lo más anchas posibles. En cuanto a componentes, lo más delicado era el amplificador diferencial THS4511, para el cual se tuvieron en cuenta las siguientes recomendaciones del fabricante:

\begin{itemize}

\item[1] El enrutamiento de la señal debe ser directo y lo más corto posible dentro y fuera del circuito amplificador operacional.
\item[2] La ruta de retroalimentación debe ser corta y directa; evitar vias.
\item[3] Los planos de tierra o de alimentación se deben quitar directamente debajo de los pines de entrada y salida del amplificador.
\item[4] Se recomienda una resistencia de salida en cada salida, lo más cerca posible del pin de salida.
\item[5] Dos condensadores de desacoplo de la fuente de alimentación de 10 \micro F y dos de 0.1 \micro F deben colocarse lo más cerca posible de los pines de la fuente de alimentación.
\item[6] Se deben colocar dos condensadores de 0.1 \micro F entre los pines de entrada CM y tierra. Esta configuración limita el ruido acoplado a los pines. Cada uno debe colocarse a tierra cerca del pin 4 y el pin 9.
\item[7] Se recomienda una conexión de un solo punto a tierra para las resistencias de terminación de entrada R1 y R2. Esta configuración debe aplicarse a las resistencias de ganancia de entrada si no se utiliza la terminación.

\end{itemize}


\section{Medidas de ruido}

Para verificar las especificaciones del diseño, se han realizado mediciones de ruido en el laboratorio y en la instalación de NEXT en el laboratorio subterráneo de Canfranc. Las medidas están relacionadas con el bit menos significativo (LSB) del sistema de adquisición de datos (DAQ). El DAQ ofrece 12 bits efectivos (ENOB) en un rango de entrada de 2V (LSB = 0,49 mV). No se tuvieron en cuenta los efectos de ruido del "dark noise" ya que su efecto es despreciable en las condiciones experimentales. Los resultados se muestran en la tabla \ref{tab:noise}.

El peor de los casos de ruido total es 0.8 LSB, que está dentro de las especificaciones. El ruido calculado teóricamente del FEE fue de 0,35 LSB sin contribución de la fuente de alimentación. La contribución de ruido del FEE medida indirectamente es 0.36 LSB, muy cerca de las especificaciones. El ruido restante se puede atribuir al sistema DAQ.

\begin{table*}
	\caption{Noise Measurements (in $LSB_{rms}$)}
	\label{tab:noise}
	
	\begin{center}
		\begin{tabular}{ c c || c | c |}
			& & Laboratory & LSC NEXT \\
			\hline
			\multirow{3}{*}{Direct Measurement} & DAQ & 0.64 & 0.66\\
			& FEE + DAQ & 0.75 & 0.75\\
			& FEE + DAQ + PMT & 0.76 & 0.8\\
			\hline
			\multirow{2}{*}{Indirect Measurement} & FE & 0.39 & 0.36\\
			& PMT & 0.12 & 0.28\\
		\end{tabular}
	\end{center}
\end{table*}



\section{Modelo del \textquotedbl Front End Electronics \textquotedbl\ para simulación}

Una vez que el diseño ha sido validado a través de mediciones reales, es de gran importancia construir un modelo de simulación que pueda ayudar en el desarrollo de otras partes del proyecto. Esta vez, el modelo tiene un alto nivel de abstracción para poder integrarse fácilmente con los módulos de análisis de software, el lenguaje Python con la ayuda de las bibliotecas Numpy-Scipy ha sido elegido como la herramienta para construirlo. El modelo comprende dos elementos diferentes, el rango de baja frecuencia que ya se ha desarrollado (ec. \ref{eq:full_fee}) más un filtro de paso bajo de cuarto orden (debido a la conformación requerida) con el factor de ganancia correcto. La respuesta del modelo se comparó con las simulaciones de SPICE del FEE implementado (\figu\ \ref{fig:Full_Freq})

\begin{figure}[H]
	\begin{center}
		\includegraphics[width=0.6\textwidth]{IMG/Chapter4/FEEfull_freq.png}
		\caption{FEE Full Frequency Response.}
		\label{fig:Full_Freq}
	\end{center}
\end{figure}

El ruido se introduce en el sistema para parecerse al comportamiento del ruido del sistema real, teniendo en cuenta el efecto de filtrado de cada parte. Esto quiere decir que, por ejemplo, el ruido de entrada equivalente de el FEE se ha incrementado para mostrar su efecto correcto dado el ancho de banda de 3 MHz. El modelo de ruido equivalente del FEE se muestra en la \figu\ \ref{fig:noise_eq} así como las ecuaciones de ruido relativas a las diferentes contribuciones de ruido.

\begin{figure}[H]
	\begin{center}
		\includegraphics[width=1\textwidth]{IMG/Chapter4/NOISE.png}
		\caption{}
		\label{fig:noise_eq}
	\end{center}
\end{figure}

\begin{align}  
GAIN &= FEE_{GAIN}.DAQ_{GAIN} \\ 
vo_{Tn}^{2} &= v_{DAQnoise}^{2}(out) + v_{F+Pn}^{2}(out) \\
vo_{Tn}^{2} &= \int_{0}^{BW=3MHz}{v_{F+Pn}^{2}.{\lvert}G.H(jw){\rvert}^2}  \\
& + \int_{0}^{BW=20MHz}{v_{DAQn}^{2}.{\lvert}DAQ_{G}.H(jw){\rvert}^2}\\  
vo_{Tn(rms)} &= \sqrt{vo_{Tn}^{2}} = 0.76LSB_{rms}
\end{align}

donde:

\begin{itemize}[label={}]
 \item $vo_{Tn}^{2}$ \textbf{is} $vo_{TOTAL_{noise}}^{2}$\\
 \item $vo_{Tn(rms)}$ \textbf{is} $vo_{TOTAL_{noise(rms)}}$\\
  \item $v_{F+Pn}^{2}$ \textbf{is} $v_{FEE+PMBnoise}^{2}$\\
 \item  ${\lvert}G.H(jw){\rvert}^{2}$ \textbf{is}  ${\lvert}GAIN.H(jw){\rvert}^{2}$\\
 \item  $v_{DAQn}^{2}$ \textbf{is} $v_{DAQnoise}^{2}$\\
  \item ${\lvert}DAQ_{G}.H(jw){\rvert}^{2}$ \textbf{is} ${\lvert}DAQ_{GAIN}.H(jw){\rvert}^{2}$
\end{itemize}

Donde la ganancia total es la ganancia del FEE multiplicada por la ganancia del sistema de adquisición. Además, el ruido total es el ruido de salida del DAQ más el ruido de la FEE, más el ruido de la base PMT.



% -------------------------------------------------
% -------------------------------------------------
% -------------------------------------------------
